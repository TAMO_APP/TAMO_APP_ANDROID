package lt.roxifas.tamo;

import android.animation.ValueAnimator;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import lt.roxifas.tamo.utils.Utils;
import lt.roxifas.tamo.models.StatusBarStateEnum;

public class StatusBarHandler {
    private MainActivity activity;

    //Views of the loading indicator bar
    private RelativeLayout loadBar;
    private ProgressBar progressBar;
    private TextView textView;
    private ImageView imageView;

    private StatusBarStateEnum currentState = StatusBarStateEnum.hidden;
    private int loadBarExtendedHeight;

    StatusBarHandler(MainActivity mainActivity) {
        activity = mainActivity;

        //Get loading indicator bar views
        loadBar = activity.drawer.findViewById(R.id.loadBar_main);
        progressBar = activity.drawer.findViewById(R.id.loadBar_spinner);
        textView = activity.drawer.findViewById(R.id.loadBar_text);
        imageView = activity.drawer.findViewById(R.id.loadBar_image);
        loadBar.measure(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        loadBarExtendedHeight = loadBar.getMeasuredHeight();
        loadBar.getLayoutParams().height = 0;
        imageView.setVisibility(View.GONE);
    }

    public void setStatus(StatusBarStateEnum state, int stringId) {
        if(!Utils.isRunningInMainThread()) {
            Utils.getInstance().runInMainThread(() -> setStatus(state, stringId));
            return;
        }
        switch (state) {
            case hidden: stateToHidden(); break;
            case loading: stateToLoading(stringId); break;
            case error: stateToError(stringId); break;
        }
        currentState = state;
    }

    private void stateToHidden() {
        concealLoadBar();
    }

    private void stateToLoading(int stringId) {
        textView.setText(stringId);
        switch (currentState) {
            case hidden: progressBar.setVisibility(View.VISIBLE); revealLoadBar(); break;
            case loading: break;
            case error: progressBar.setVisibility(View.VISIBLE); imageView.setVisibility(View.INVISIBLE); break;
            default: Toast.makeText(activity, R.string.info_unhandledInfoTransition, Toast.LENGTH_LONG).show(); break;
        }
    }

    private void stateToError(int stringId) {
        textView.setText(stringId);
        switch (currentState) {
            case hidden:
                imageView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                revealLoadBar();
                break;
            case loading: progressBar.setVisibility(View.INVISIBLE); imageView.setVisibility(View.VISIBLE); break;
            case error: break;
            default: Toast.makeText(activity, R.string.info_unhandledInfoTransition, Toast.LENGTH_LONG).show(); break;
        }
    }

    private void revealLoadBar() {
        ValueAnimator va = ValueAnimator.ofInt(0, loadBarExtendedHeight);
        va.setDuration(400);
        va.addUpdateListener(animation -> {
            loadBar.getLayoutParams().height = (Integer) animation.getAnimatedValue();
            loadBar.requestLayout();
        });
        va.start();
    }

    private void concealLoadBar() {
        final int initialHeight = loadBar.getMeasuredHeight();
        ValueAnimator va = ValueAnimator.ofInt(initialHeight, 0);
        va.setDuration(400);
        va.addUpdateListener(animation -> {
            loadBar.getLayoutParams().height = (Integer) animation.getAnimatedValue();
            loadBar.requestLayout();
        });
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(() -> {
            imageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        },400);

        va.start();
    }

}
