package lt.roxifas.tamo;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import cat.ereza.customactivityoncrash.config.CaocConfig;

public class CrashHandlerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash_handler);
        final CaocConfig caocConfig = CustomActivityOnCrash.getConfigFromIntent(getIntent());
        //Get error details
        final String crashLog = CustomActivityOnCrash.getAllErrorDetailsFromIntent(CrashHandlerActivity.this, getIntent());
        //Set the stack trace as textView's text
        ((TextView)findViewById(R.id.crashActivity_stackTraceText)).setText(crashLog);
        ((TextView)findViewById(R.id.crashActivity_stackTraceText)).setMovementMethod(new ScrollingMovementMethod());
        //Set click handler to close button
        findViewById(R.id.crashActivity_closeButton).setOnClickListener(v ->
                CustomActivityOnCrash.closeApplication(CrashHandlerActivity.this, caocConfig)
        );
        //Set click handler to copy button
        findViewById(R.id.crashActivity_copyButton).setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) CrashHandlerActivity.this.getSystemService(Context.CLIPBOARD_SERVICE);
            assert clipboard != null;
            clipboard.setPrimaryClip(ClipData.newPlainText(CrashHandlerActivity.this.getString(R.string.exception_parser), crashLog));
        });
        //Set click handler to restart button
        findViewById(R.id.crashActivity_restartButton).setOnClickListener(v ->
                CustomActivityOnCrash.restartApplication(CrashHandlerActivity.this, caocConfig)
        );
    }
}
