package lt.roxifas.tamo.sections;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import lt.roxifas.tamo.MainActivity;
import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.StatusBarStateEnum;
import lt.roxifas.tamo.models.dataItems.AccountModel;
import lt.roxifas.tamo.models.SectionModel;

public class Login extends SectionModel implements View.OnClickListener {
    private RelativeLayout loginLayout;
    private EditText nameInput;
    private EditText passInput;
    private Button button;
    private boolean sectionClosed = false;

    public Login(MainActivity _activity) {
        super(_activity);
    }

    public void sectionInit() {
        sectionClosed = false;

        LayoutInflater inflater = activity.getLayoutInflater();
        loginLayout = (RelativeLayout) inflater.inflate(R.layout.layout_login, activity.mainLayout, false);
        loginLayout.setLayoutParams(activity.contentParams);
        activity.addView(loginLayout);

        //Get references to views
        button = loginLayout.findViewById(R.id.loginButton);
        nameInput = loginLayout.findViewById(R.id.usernameInput);
        passInput = loginLayout.findViewById(R.id.userpassInput);
        //Add onClick handler to login button
        button.setOnClickListener(this);
    }

    public void sectionClose() {
        if(sectionClosed) return;
        sectionClosed = true;
        activity.removeView(loginLayout);
    }

    @Override
    public void onClick(View v) {
        String username = nameInput.getText().toString();
        button.setEnabled(false);
        AccountModel account = activity.services.accountsService.getAccountByUsername(username);
        if(account != null) {
            activity.services.accountsService.activeId.set(account.getId());
            activity.hideKeyboard();
            sectionClose();
            return;
        }
        String password = passInput.getText().toString();
        activity.statusBarHandler.setStatus(StatusBarStateEnum.loading, R.string.info_loggingIn);
        activity.services.dataService.tryNewAccount(username, password).done(result -> {
            activity.hideKeyboard();
            sectionClose();
            activity.statusBarHandler.setStatus(StatusBarStateEnum.hidden, 0);
        }).fail(result -> {
            button.setEnabled(true);
            activity.statusBarHandler.setStatus(StatusBarStateEnum.error, R.string.loginFailed_generic);
        });
    }
}

