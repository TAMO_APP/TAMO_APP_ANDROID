package lt.roxifas.tamo.sections;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.jdeferred2.DoneCallback;

import java.util.ArrayList;

import lt.roxifas.tamo.MainActivity;
import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.NameIdPair;
import lt.roxifas.tamo.models.SectionModel;
import lt.roxifas.tamo.models.dataItems.SemesterModel;

public class Semesters extends SectionModel {
    private SemestersListAdapter adapter;
    private ArrayList<SemesterModel> semesters = new ArrayList<>();
    private ArrayList<NameIdPair> choices = new ArrayList<>();
    private ArrayList<String> choiceNames = new ArrayList<>();
    private int currentChoice;

    public Semesters(MainActivity _Activity) {
        super(_Activity);
        setName(R.string.sectionName_Semesters);
        setIcon(R.drawable.ic_calendar_range);
        setGroup(sectionGroup);
        adapter = new SemestersListAdapter();
    }

    private DoneCallback<ArrayList<SemesterModel>> doneCallback = result -> {
        semesters = result;
        adapter.notifyDataSetChanged();
    };

    public void sectionInit() {
        doneCallback.onDone(activity.services.dataService.getCachedSemester());
        activity.services.dataService.getNewSemester().done(result -> {
            semesters = result.first;
            adapter.notifyDataSetChanged();
            choices = result.second;
            choiceNames.clear();
            for(NameIdPair pair : choices) {
                choiceNames.add(pair.name);
                if(pair.selected) {
                    currentChoice = choices.indexOf(pair);
                }
            }
            makeMenu();
        });
        //Setup RecyclerView
        activity.recyclerView.setAdapter(adapter);
        activity.showView(activity.recyclerView);
    }

    public void sectionClose() {
        activity.hideView(activity.recyclerView);
        activity.hideView(activity.actionBarDropDown);
    }

    private class SemestersListAdapter extends RecyclerView.Adapter<SemestersListAdapter.SemesterView> {
        LayoutInflater inflater = activity.getLayoutInflater();

        @NonNull
        @Override
        public SemesterView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.element_entry_with_subtext, parent, false);
            return new SemesterView(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SemesterView holder, int position) {
            holder.gradesText.setText(semesters.get(position).grades);
            holder.subjectText.setText(semesters.get(position).subjectAverages);
        }

        @Override
        public int getItemCount() {
            return semesters.size();
        }

        class SemesterView extends RecyclerView.ViewHolder {
            TextView subjectText;
            TextView gradesText;
            SemesterView(View itemView) {
                super(itemView);
                subjectText = itemView.findViewById(R.id.subtextEntry_main);
                gradesText = itemView.findViewById(R.id.subtextEntry_sub);
            }
        }
    }

    private void makeMenu() {
        activity.actionBarDropDownAdapter = new ArrayAdapter<>(activity, R.layout.custom_spinner_root, choiceNames);
        activity.actionBarDropDownAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activity.actionBarDropDown.setAdapter(activity.actionBarDropDownAdapter);
        activity.actionBarDropDown.setSelection(currentChoice);
        activity.showView(activity.actionBarDropDown);
        activity.actionBarDropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (currentChoice != i) {
                    activity.services.dataService.getFilteredSemester(choices.get(i).id).done(doneCallback);
                    currentChoice = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

}
