package lt.roxifas.tamo.sections;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import org.jdeferred2.DoneCallback;

import java.util.ArrayList;

import lt.roxifas.tamo.MainActivity;
import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.SectionModel;
import lt.roxifas.tamo.models.dataItems.NameIdPair;
import lt.roxifas.tamo.models.dataItems.TestModel;
import lt.roxifas.tamo.models.viewHolders.TestHolder;
import lt.roxifas.tamo.utils.CommonRecyclerAdapter;
import lt.roxifas.tamo.utils.HeaderItemDecoration;

public class Tests extends SectionModel {
    private CommonRecyclerAdapter<TestHolder, TestModel> adapter;
    private HeaderItemDecoration headerItemDecoration;
    private ArrayList<NameIdPair> choices = new ArrayList<>();
    private ArrayList<String> choiceNames = new ArrayList<>();
    private int activeChoiceIndex;

    public Tests (MainActivity _activity) {
        super(_activity);
        setName(R.string.sectionName_Tests);
        setIcon(R.drawable.ic_checkmark_outline);
        setGroup(sectionGroup);
        adapter = new CommonRecyclerAdapter<>(R.layout.element_entry_with_subtext, activity, TestHolder.class);
    }

    private DoneCallback<ArrayList<TestModel>> doneCallback = result -> {
        ArrayList<TestModel> testEntries = result;
        testEntries = HeaderItemDecoration.makeHeaders(testEntries, activity.getString(R.string.helper_emptyList), TestModel.class);
        adapter.setSourceList(testEntries);
    };

    public void sectionInit() {
        doneCallback.onDone(activity.services.dataService.getCachedTests());
        activity.services.dataService.getNewTests().done(result -> {
            ArrayList<TestModel> testEntries = result.first;
            testEntries = HeaderItemDecoration.makeHeaders(testEntries, activity.getString(R.string.helper_emptyList), TestModel.class);
            adapter.setSourceList(testEntries);
            choices = result.second;
            for(NameIdPair pair : choices) {
                choiceNames.add(pair.name);
                if(pair.selected) {
                    activeChoiceIndex = choices.indexOf(pair);
                }
            }
            makeMenu();
        });

        activity.recyclerView.setAdapter(adapter);
        headerItemDecoration = new HeaderItemDecoration(adapter, activity.recyclerView);
        activity.recyclerView.addItemDecoration(headerItemDecoration);
        activity.showView(activity.recyclerView);
    }

    public void sectionClose() {
        activity.hideView(activity.recyclerView);
        activity.recyclerView.removeItemDecoration(headerItemDecoration);
        activity.hideView(activity.actionBarDropDown);
    }

    private void makeMenu() {
        activity.actionBarDropDownAdapter = new ArrayAdapter<>(activity, R.layout.custom_spinner_root, choiceNames);
        activity.actionBarDropDownAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activity.actionBarDropDown.setAdapter(activity.actionBarDropDownAdapter);
        activity.actionBarDropDown.setSelection(activeChoiceIndex);
        activity.showView(activity.actionBarDropDown);
        activity.actionBarDropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == activeChoiceIndex) return;
                activity.services.dataService.getFilteredTests(choices.get(position).id).done(doneCallback);
                activeChoiceIndex = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}
