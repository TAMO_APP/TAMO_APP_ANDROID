package lt.roxifas.tamo.sections;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.text.util.Linkify;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.jdeferred2.DoneCallback;

import java.util.ArrayList;

import lt.roxifas.tamo.MainActivity;
import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.FileRequestModel;
import lt.roxifas.tamo.utils.Utils;
import lt.roxifas.tamo.models.dataItems.MessageListItemModel;
import lt.roxifas.tamo.models.dataItems.MessageModel;
import lt.roxifas.tamo.models.SectionModel;

public class Messages extends SectionModel {

    private MessageModel currentMessage;
    private int currentMessagePosition;
    private ArrayList<MessageListItemModel> messages = new ArrayList<>();
    private messageListAdapter adapter;
    private TextView noMessageText;
    private boolean messageViewActive = false;
    private TextView messageText;
    private int pagesAvailable = 0, pagesLoaded = 1;
    private boolean loadingMessages = false;

    private DoneCallback<Pair<ArrayList<MessageListItemModel>, Integer>> doneCallback = result -> {
        messages = result.first;
        pagesLoaded = 1;
        updateList();
        pagesAvailable = result.second;
    };

    private class messageListAdapter extends RecyclerView.Adapter<messageListAdapter.viewHolder> {
        private LayoutInflater inflater;
        private boolean selectionModeActive = false;
        private int selectedItemCount = 0;
        private ArrayList<View> messageViews = new ArrayList<>();

        private final int markedColor = ContextCompat.getColor(activity, android.R.color.holo_blue_light);
        private final int normalColor = Color.parseColor("#FAFAFA"); //Default background color (it's not white and animations would get weird if it's not exactly this color)

        messageListAdapter() {
            inflater = activity.getLayoutInflater();
        }

        @NonNull
        @Override
        public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.element_message, parent, false);
            messageViews.add(view);
            return new viewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull viewHolder holder, @SuppressLint("RecyclerView") int position) {
            holder.titleText.setText(messages.get(position).title);
            holder.senderText.setText(messages.get(position).sender);
            holder.dateText.setText(messages.get(position).date);
            holder.listingNumber = position;
            if(messages.get(position).selected)holder.itemView.setBackgroundColor(markedColor);
            else holder.itemView.setBackgroundColor(normalColor);

            if(messages.get(position).hasAttachment)holder.attachmentIndicator.setVisibility(View.VISIBLE);
            else holder.attachmentIndicator.setVisibility(View.GONE);

            if(!messages.get(position).isRead) {
                holder.titleText.setTypeface(null, Typeface.BOLD);
                holder.senderText.setTypeface(null, Typeface.BOLD);
                holder.dateText.setTypeface(null, Typeface.BOLD);
            } else {
                holder.titleText.setTypeface(null, Typeface.NORMAL);
                holder.senderText.setTypeface(null, Typeface.NORMAL);
                holder.dateText.setTypeface(null, Typeface.NORMAL);
            }

            if((position + 1) == messages.size())loadMoreMessages();
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }

        class viewHolder extends RecyclerView.ViewHolder {
            View itemView;
            TextView titleText;
            TextView senderText;
            TextView dateText;
            ImageView attachmentIndicator;
            int listingNumber;

            viewHolder(final View input) {
                super(input);

                itemView = input;

                titleText = itemView.findViewById(R.id.message_title);
                senderText = itemView.findViewById(R.id.message_sender);
                dateText = itemView.findViewById(R.id.message_date);
                attachmentIndicator = itemView.findViewById(R.id.message_attachment);
                itemView.setOnClickListener(view -> {
                    if(messages.get(listingNumber).selected) { //Take action when the message is selected
                        messages.get(listingNumber).selected = false;
                        colorAnimation(markedColor, normalColor, itemView);
                        selectedItemCount--;
                        if(selectedItemCount == 0)setSelectionModeActive(false);
                    } else if(selectionModeActive) { //Take action when selection mode is active (and the message is not selected)
                        messages.get(listingNumber).selected = true;
                        colorAnimation(normalColor, markedColor, itemView);
                        selectedItemCount++;
                    } else { //Take action when selection mode is inactive, therefore opening the message
                        currentMessagePosition = listingNumber;
                        activity.services.dataService.getMessage(messages.get(listingNumber).identificator).done(result -> {
                            currentMessage = result;
                            currentMessage.listingPosition = currentMessagePosition;
                            showMessageView();
                        });
                        messageViewActive = true;
                        activity.hideView(activity.recyclerView);
                    }
                });

                itemView.setOnLongClickListener(view -> {
                    messages.get(listingNumber).selected = !messages.get(listingNumber).selected;
                    if(messages.get(listingNumber).selected) {
                        colorAnimation(normalColor, markedColor, itemView);
                        selectedItemCount++;
                        if(selectedItemCount == 1)setSelectionModeActive(true);
                    } else {
                        colorAnimation(markedColor, normalColor, itemView);
                        selectedItemCount--;
                        if(selectedItemCount == 0)setSelectionModeActive(false);
                    }
                    return true;
                });

            }
        }

        private void setSelectionModeActive(boolean isActive) {
            if(isActive) {
                selectionModeActive = true;
                activity.actionBarMenu.add(Menu.NONE, 1, Menu.NONE, R.string.button_deleteSelectedMessages).setIcon(R.drawable.ic_delete).setOnMenuItemClickListener(menuItem -> {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(R.string.button_deleteSelectedMessages);
                    builder.setMessage(activity.getResources().getQuantityString(R.plurals.helper_messageDeleteConfirmationMultiple,selectedItemCount, selectedItemCount));
                    builder.setNegativeButton(R.string.button_cancel, (dialogInterface, i) -> {

                    });
                    builder.setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                        deleteMultipleMessages();
                        selectedItemCount = 0;
                        setSelectionModeActive(false);
                        activity.services.dataService.getNewMessages().done(doneCallback);
                    });
                    builder.create().show();
                    return false;
                }).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            } else {
                selectionModeActive = false;
                activity.actionBarMenu.removeItem(1);
                selectedItemCount = 0;

                //Deselect all items
                for(int i = 0 ; i < messages.size() ; i++) {
                    if(messages.get(i).selected) {
                        colorAnimation(markedColor, normalColor, messageViews.get(i));
                        messages.get(i).selected = false;
                    }
                }
                notifyDataSetChanged();
            }
        }

        boolean isSelectionActive() {
            return selectionModeActive;
        }

        private void deleteMultipleMessages() {
            for(int i = 0 ; i < messages.size() ; i++) {
                if(messages.get(i).selected) {
                    deleteMessage(messages.get(i));
                    i--;
                }
            }
        }

    }

    public Messages(MainActivity _activity) {
        super(_activity);
        setName(R.string.sectionName_Messages);
        setIcon(R.drawable.ic_mail);
        setGroup(sectionGroup);
        adapter = new messageListAdapter();
        messageText = new TextView(activity);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(activity.contentParams);
        Utils utils = Utils.getInstance();
        params.setMargins(utils.dpToPx(5), 0, utils.dpToPx(5), 0);
        messageText.setLayoutParams(params);
        //Makes textView scrollable, but not with kinetic scroll
        messageText.setVerticalScrollBarEnabled(true);
        messageText.setMovementMethod(ScrollingMovementMethod.getInstance());
    }

    public void sectionInit() {
        //Get cached messages
        messages = activity.services.dataService.getCachedMessages();
        updateList();
        //Get new messages and prepare views
        activity.services.dataService.getNewMessages().done(doneCallback);
        activity.recyclerView.setAdapter(adapter);
        activity.showView(activity.recyclerView);
    }

    public void sectionClose() {
        if(noMessageText != null) activity.removeView(noMessageText);
        activity.hideView(activity.recyclerView);
        if(messageViewActive)hideMessageView();
    }

    @SuppressLint("InflateParams")
    private void updateList() {
        if(messages.isEmpty()) {
            if(noMessageText == null) {
                noMessageText = (TextView) activity.getLayoutInflater().inflate(R.layout.element_header, null, false);
            }
            if(activity.mainLayout.indexOfChild(noMessageText) == -1) {
                noMessageText.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                noMessageText.setText(R.string.helper_noMessages);
                activity.hideView(activity.recyclerView);
                activity.addView(noMessageText);
            }
        } else {
            if(noMessageText != null && activity.mainLayout.indexOfChild(noMessageText) != -1) {
                activity.removeView(noMessageText);
                activity.showView(activity.recyclerView);
            }
            adapter.notifyDataSetChanged();
        }
    }

    private void buildAttachmentMenu() {
        final AlertDialog.Builder builderSingle = new AlertDialog.Builder(activity);
        builderSingle.setTitle(R.string.dialog_messageAttachments);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1);

        for (String attachmentName : currentMessage.attachmentNames) {
            arrayAdapter.add(attachmentName);
        }

        builderSingle.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss());

        builderSingle.setAdapter(arrayAdapter, (dialog, position) -> {
            FileRequestModel requestModel = new FileRequestModel();
            requestModel.filename = currentMessage.attachmentNames.get(position);
            requestModel.address = currentMessage.attachmentId.get(position);
            activity.services.dataService.downloadFile(requestModel);
        });
        activity.actionBarMenu.add(Menu.NONE, 2, Menu.NONE, R.string.dialog_messageAttachments)
                .setIcon(R.drawable.ic_attachment_white)
                .setOnMenuItemClickListener(menuItem -> {
                    Utils.getInstance().ensurePermissionAvailable(Manifest.permission.WRITE_EXTERNAL_STORAGE).done(result ->
                            builderSingle.show()
                    ).fail(result ->
                            Toast.makeText(activity, R.string.error_noFilePermission, Toast.LENGTH_LONG).show()
                    );
                    return false;
                }
                ).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    private void colorAnimation(int first, int second, View viewToAnimate) {
        ColorDrawable[] color = {new ColorDrawable(first), new ColorDrawable(second)}; //Create transition points
        TransitionDrawable trans = new TransitionDrawable(color); //Create drawable for transition
        viewToAnimate.setBackground(trans);
        trans.startTransition(300); //Start animation
    }

    private void showMessageView() {
        //Mark message as read (in the active list, then in the cache db)
        messages.get(currentMessage.listingPosition).isRead = true;
        activity.services.dataService.markMessageAsRead(messages.get(currentMessage.listingPosition).identificator);
        adapter.notifyDataSetChanged();
        messageText.setText(currentMessage.text);
        Linkify.addLinks(messageText, Utils.linkPattern,"");
        //Add menu option to delete the current message
        activity.actionBarMenu.add(Menu.NONE, 1, Menu.NONE, R.string.button_deleteMessage).setIcon(R.drawable.ic_delete).setOnMenuItemClickListener(menuItem -> {
            deleteMessage(messages.get(currentMessage.listingPosition));
            hideMessageView();
            activity.showView(activity.recyclerView);
            activity.services.dataService.getNewMessages().done(doneCallback);
            return false;
        }).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        activity.addView(messageText);
        if(currentMessage.attachmentCount > 0) buildAttachmentMenu();
        activity.setTitle(messages.get(currentMessage.listingPosition).title);
        //Update the unread counter in the navigation drawer
        activity.setUnreadMessageCount(countUnreadMessages());
    }

    private void deleteMessage(MessageListItemModel message) {
        activity.services.dataService.deleteMessage(message.identificator);
        messages.remove(message);
        adapter.notifyItemRemoved(messages.indexOf(message));
        adapter.notifyItemRangeChanged(0, messages.size() + 1);
    }

    private void hideMessageView() {
        activity.setTitle(R.string.sectionName_Messages);
        messageViewActive = false;
        activity.removeView(messageText);
        activity.actionBarMenu.removeItem(1);
        activity.actionBarMenu.removeItem(2);
    }

    public boolean onBackButton() {
        if(adapter.isSelectionActive()) {
            adapter.setSelectionModeActive(false);
            return false;//Do not close message section
        }
        if(messageViewActive) {
            hideMessageView();
            activity.showView(activity.recyclerView);
            return false;//Do not close message section
        }

        return true;//Closes message section
    }

    private void loadMoreMessages() {
        if(pagesAvailable <= pagesLoaded || loadingMessages)return;
        loadingMessages = true;
        activity.services.dataService.getMessagePage(++pagesLoaded).done(result -> {
            messages.addAll(result);
            loadingMessages = false;
            adapter.notifyDataSetChanged();
        });
        Toast.makeText(activity, R.string.info_loadingMessages, Toast.LENGTH_LONG).show();

    }

    private int countUnreadMessages() {
        int unreadCount = 0;
        for(MessageListItemModel message : messages) {
            if(!message.isRead)unreadCount++;
        }
        return unreadCount;
    }

}
