package lt.roxifas.tamo.sections;

import org.jdeferred2.DoneCallback;

import java.util.ArrayList;

import lt.roxifas.tamo.utils.HeaderItemDecoration;
import lt.roxifas.tamo.utils.CommonRecyclerAdapter;
import lt.roxifas.tamo.MainActivity;
import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.TimetableModel;
import lt.roxifas.tamo.models.SectionModel;
import lt.roxifas.tamo.models.viewHolders.TimetableHolder;

public class Timetable extends SectionModel {
    private CommonRecyclerAdapter<TimetableHolder, TimetableModel> adapter;
    private ArrayList<TimetableModel> entryList = new ArrayList<>();
    private HeaderItemDecoration headerItemDecoration;

    public Timetable(MainActivity _activity) {
        super(_activity);
        setName(R.string.sectionName_Timetable);
        setIcon(R.drawable.ic_timetable);
        setGroup(sectionGroup);
        adapter = new CommonRecyclerAdapter<>(R.layout.element_entry_with_subtext, activity, TimetableHolder.class);
    }

    private DoneCallback<ArrayList<TimetableModel>> doneCallback = result -> {
        entryList = result;
        entryList = HeaderItemDecoration.makeHeaders(entryList, activity.getString(R.string.helper_emptyList), TimetableModel.class);
        adapter.setSourceList(entryList);
    };

    public void sectionInit() {
        doneCallback.onDone(activity.services.dataService.getCachedTimetable());
        activity.services.dataService.getNewTimetable().done(doneCallback);
        activity.recyclerView.setAdapter(adapter);
        headerItemDecoration = new HeaderItemDecoration(adapter, activity.recyclerView);
        activity.recyclerView.addItemDecoration(headerItemDecoration);
        activity.showView(activity.recyclerView);
    }

    public void sectionClose() {
        activity.hideView(activity.recyclerView);
        activity.recyclerView.removeItemDecoration(headerItemDecoration);
    }

}
