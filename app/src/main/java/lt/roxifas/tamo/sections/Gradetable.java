package lt.roxifas.tamo.sections;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import org.jdeferred2.DoneCallback;

import java.util.ArrayList;

import lt.roxifas.tamo.utils.HeaderItemDecoration;
import lt.roxifas.tamo.utils.CommonRecyclerAdapter;
import lt.roxifas.tamo.MainActivity;
import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.GradeModel;
import lt.roxifas.tamo.models.SectionModel;
import lt.roxifas.tamo.models.viewHolders.GradeHolder;

public class Gradetable extends SectionModel {
    private ArrayList<GradeModel> gradeList = new ArrayList<>();
    private CommonRecyclerAdapter<GradeHolder, GradeModel> adapter;
    private int currentChoice;
    private ArrayList<String> choices = new ArrayList<>();
    private HeaderItemDecoration headerItemDecoration;

    //Section control
    public Gradetable(MainActivity _activity) {
        super(_activity);
        setName(R.string.sectionName_Gradetable);
        setIcon(R.drawable.ic_table);
        setGroup(sectionGroup);
        adapter = new CommonRecyclerAdapter<>(R.layout.element_grade, activity, GradeHolder.class);
    }

    private DoneCallback<ArrayList<GradeModel>> doneCallback = result -> {
        gradeList = result;
        gradeList = HeaderItemDecoration.makeHeaders(gradeList, activity.getString(R.string.helper_emptyList), GradeModel.class);
        adapter.setSourceList(gradeList);
    };

    public void sectionInit() {
        doneCallback.onDone(activity.services.dataService.getCachedGrades());
        activity.services.dataService.getNewGrades().done(result -> {
            gradeList = result.first;
            gradeList = HeaderItemDecoration.makeHeaders(gradeList, activity.getString(R.string.helper_emptyList), GradeModel.class);
            adapter.setSourceList(gradeList);
            choices = result.second;
            currentChoice = choices.size()-1;
            makeMenu();
        });

        //Initialize the recycler view
        activity.recyclerView.setAdapter(adapter);
        headerItemDecoration = new HeaderItemDecoration(adapter, activity.recyclerView);
        activity.recyclerView.addItemDecoration(headerItemDecoration);
        activity.showView(activity.recyclerView);
    }

    public void sectionClose() {
        activity.hideView(activity.recyclerView);
        activity.recyclerView.removeItemDecoration(headerItemDecoration);
        activity.hideView(activity.actionBarDropDown);
        gradeList.clear();
    }

    private void makeMenu() {
        activity.actionBarDropDownAdapter = new ArrayAdapter<>(activity, R.layout.custom_spinner_root,choices);
        activity.actionBarDropDownAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activity.actionBarDropDown.setAdapter(activity.actionBarDropDownAdapter);
        activity.actionBarDropDown.setSelection(currentChoice);
        activity.actionBarDropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(currentChoice == i)return;
                currentChoice = i;
                activity.services.dataService.getFilteredGrades(choices.get(i)).done(doneCallback);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        activity.showView(activity.actionBarDropDown);
    }

}
