package lt.roxifas.tamo.sections;

import android.widget.Toast;

import java.util.ArrayList;

import lt.roxifas.tamo.MainActivity;
import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.SectionModel;
import lt.roxifas.tamo.models.dataItems.DevToolItem;
import lt.roxifas.tamo.models.viewHolders.DevToolHolder;
import lt.roxifas.tamo.utils.CommonRecyclerAdapter;
import lt.roxifas.tamo.utils.Utils;

public class DevTools extends SectionModel {
    private CommonRecyclerAdapter<DevToolHolder, DevToolItem> adapter;
    private ArrayList<DevToolItem> list = new ArrayList<>();

    public DevTools(MainActivity _activity) {
        super(_activity);
        setName(R.string.sectionName_DevTools);
        setIcon(R.drawable.ic_bug_grey);
        setGroup(settingsGroup);
        adapter = new CommonRecyclerAdapter<>(R.layout.element_text, activity, DevToolHolder.class);
        Utils.getInstance().runInMainThread(() -> {
            prepareList();
            adapter.setSourceList(list);
        });
    }

    @Override
    public void sectionInit() {
        activity.recyclerView.setAdapter(adapter);
        activity.showView(activity.recyclerView);
    }

    @Override
    public void sectionClose() {
        activity.hideView(activity.recyclerView);
    }

    private void prepareList() {
        //Cache clear
        DevToolItem item = new DevToolItem();
        item.textResource = R.string.devTools_clearCache;
        item.listener = v -> {
            activity.services.deleteCurrentCachedData();
            Toast.makeText(activity, R.string.info_cacheCleared, Toast.LENGTH_LONG).show();
        };
        list.add(item);
        //Session clear
        item = new DevToolItem();
        item.textResource = R.string.devTools_resetSession;
        item.listener = v -> {
            activity.services.accountsService.activeSession.get().invalidate();
            Toast.makeText(activity, R.string.info_sessionReset, Toast.LENGTH_LONG).show();
        };
        list.add(item);
    }

}
