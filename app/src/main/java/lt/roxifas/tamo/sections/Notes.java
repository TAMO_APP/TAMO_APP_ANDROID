package lt.roxifas.tamo.sections;

import lt.roxifas.tamo.MainActivity;
import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.NoteModel;
import lt.roxifas.tamo.models.viewHolders.NoteHolder;
import lt.roxifas.tamo.utils.CommonRecyclerAdapter;
import lt.roxifas.tamo.models.SectionModel;

public class Notes extends SectionModel {
    private CommonRecyclerAdapter<NoteHolder, NoteModel> listAdapter;

    public Notes(MainActivity _activity) {
        super(_activity);
        setName(R.string.sectionName_Notes);
        setIcon(R.drawable.ic_notes);
        setGroup(sectionGroup);
        listAdapter = new CommonRecyclerAdapter<>(R.layout.element_notes, activity, NoteHolder.class);
    }

    public void sectionInit() {
        listAdapter.setSourceList(activity.services.dataService.getCachedNotes());
        activity.services.dataService.getNewNotes().done(result -> listAdapter.setSourceList(result));

        activity.recyclerView.setAdapter(listAdapter);
        activity.showView(activity.recyclerView);
    }

    public void sectionClose() {
        activity.hideView(activity.recyclerView);
    }

}
