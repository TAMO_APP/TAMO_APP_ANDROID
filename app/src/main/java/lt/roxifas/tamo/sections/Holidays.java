package lt.roxifas.tamo.sections;

import lt.roxifas.tamo.MainActivity;
import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.SectionModel;
import lt.roxifas.tamo.models.dataItems.HolidayModel;
import lt.roxifas.tamo.models.viewHolders.HolidayHolder;
import lt.roxifas.tamo.utils.CommonRecyclerAdapter;

public class Holidays extends SectionModel {
    private CommonRecyclerAdapter<HolidayHolder, HolidayModel> adapter;

    public Holidays(MainActivity _activity) {
        super(_activity);
        setName(R.string.sectionName_Holidays);
        setIcon(R.drawable.ic_calendar);
        setGroup(sectionGroup);
        adapter = new CommonRecyclerAdapter<>(R.layout.element_holiday, activity, HolidayHolder.class);
    }

    public void sectionInit() {
        adapter.setSourceList(activity.services.dataService.getCachedHolidays());
        activity.services.dataService.getNewHolidays().done(result -> adapter.setSourceList(result));

        //Setup RecyclerView
        activity.recyclerView.setAdapter(adapter);
        activity.showView(activity.recyclerView);
    }

    public void sectionClose() {
        activity.hideView(activity.recyclerView);
    }

}
