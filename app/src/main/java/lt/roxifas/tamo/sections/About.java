package lt.roxifas.tamo.sections;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import lt.roxifas.tamo.MainActivity;
import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.AboutItemModel;
import lt.roxifas.tamo.models.viewHolders.AboutHolder;
import lt.roxifas.tamo.utils.CommonRecyclerAdapter;
import lt.roxifas.tamo.models.SectionModel;
import lt.roxifas.tamo.utils.Utils;

public class About extends SectionModel {
    private CommonRecyclerAdapter<AboutHolder, AboutItemModel> adapter;
    private ArrayList<AboutItemModel> list = new ArrayList<>();
    private ArrayList<String> libraryNames = new ArrayList<>();
    private ArrayList<Integer> libraryLicenses = new ArrayList<>();

    public About(MainActivity _activity) {
        super(_activity);
        setName(R.string.sectionName_About);
        setIcon(R.drawable.ic_help);
        setGroup(settingsGroup);
        adapter = new CommonRecyclerAdapter<>(R.layout.element_about, activity, AboutHolder.class);
        Utils.getInstance().runInMainThread(() -> {
            populateOpenSourceLibraryList();
            prepareList();
            adapter.setSourceList(list);
        });
    }

    public void sectionInit() {
        activity.recyclerView.setAdapter(adapter);
        activity.showView(activity.recyclerView);
    }

    public void sectionClose() {
        activity.hideView(activity.recyclerView);
    }

    private void prepareList() {
        Resources resources = activity.getResources();
        //Application's license item
        AboutItemModel item = new AboutItemModel();
        item.textResource = R.string.about_license;
        item.imageResource = R.drawable.ic_notes;
        item.listener = v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.about_license);
            builder.setIcon(R.drawable.ic_notes);
            builder.setMessage(Utils.readInputStream(resources.openRawResource(R.raw.app_license)));
            builder.setPositiveButton(R.string.button_ok, null);
            builder.show();
        };
        list.add(item);
        //Link to repository
        item = new AboutItemModel();
        item.textResource = R.string.about_source;
        item.imageResource = R.drawable.ic_gitlab;
        item.listener = v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/TAMO_APP/TAMO_APP_ANDROID"));
            activity.startActivity(browserIntent);
        };
        list.add(item);
        //Open source library licenses
        item = new AboutItemModel();
        item.textResource = R.string.about_libraries;
        item.imageResource = R.drawable.ic_codetags;
        item.listener = v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, libraryNames);
            builder.setTitle(R.string.about_libraries);
            builder.setIcon(R.drawable.ic_notes);
            builder.setAdapter(adapter, (dialog, which) -> {
                AlertDialog.Builder textBuilder = new AlertDialog.Builder(activity);
                textBuilder.setTitle(libraryNames.get(which));
                textBuilder.setIcon(R.drawable.ic_notes);
                textBuilder.setMessage(Utils.readInputStream(resources.openRawResource(libraryLicenses.get(which))));
                textBuilder.setPositiveButton(R.string.button_ok, null);
                textBuilder.show();
            });
            builder.setPositiveButton(R.string.button_ok, null);
            builder.show();
        };
        list.add(item);
    }

    private void populateOpenSourceLibraryList() {
        libraryNames.add("jsoup");
        libraryLicenses.add(R.raw.jsoup_license);
        libraryNames.add("jdeffered");
        libraryLicenses.add(R.raw.apache2_generic_license);
        libraryNames.add("CustomActivityOnCrash");
        libraryLicenses.add(R.raw.apache2_generic_license);
    }

}
