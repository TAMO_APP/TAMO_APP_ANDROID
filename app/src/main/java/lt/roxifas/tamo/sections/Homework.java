package lt.roxifas.tamo.sections;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import org.jdeferred2.DoneCallback;

import java.util.Calendar;
import java.util.ArrayList;

import lt.roxifas.tamo.models.viewHolders.HomeworkHolder;
import lt.roxifas.tamo.utils.DatePickerFragment;
import lt.roxifas.tamo.utils.HeaderItemDecoration;
import lt.roxifas.tamo.utils.CommonRecyclerAdapter;
import lt.roxifas.tamo.MainActivity;
import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.HomeworkRequestModel;
import lt.roxifas.tamo.models.dataItems.NameIdPair;
import lt.roxifas.tamo.models.dataItems.HomeworkModel;
import lt.roxifas.tamo.models.SectionModel;

import static lt.roxifas.tamo.utils.Utils.dateFormat;


public class Homework extends SectionModel implements DatePickerDialog.OnDateSetListener {
    private CommonRecyclerAdapter<HomeworkHolder, HomeworkModel> homeworkViewAdapter;
    private ArrayList<HomeworkModel> homeworkList = new ArrayList<>();
    private int datePickerMode;
    private Dialog datePickerDialog;
    private HeaderItemDecoration headerItemDecoration;

    private ArrayList<NameIdPair> subjects;

    private HomeworkRequestModel postDataset = new HomeworkRequestModel();
    private HomeworkRequestModel tempPostDataset = new HomeworkRequestModel();

    public Homework(MainActivity _activity) {
        super(_activity);
        setName(R.string.sectionName_Homework);
        setIcon(R.drawable.ic_list);
        setGroup(sectionGroup);
        homeworkViewAdapter = new CommonRecyclerAdapter<>(R.layout.element_homework, activity, HomeworkHolder.class);
    }

    private class subjectListAdapter extends BaseAdapter {
        private LayoutInflater inflater = activity.getLayoutInflater();

        @Override
        public int getCount() {
            return subjects.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView;
            if(convertView == null) {
                rowView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
            } else {
                rowView = convertView;
            }
            ((TextView)rowView).setText(subjects.get(position).name);
            return rowView;

        }

    }

    private DoneCallback<ArrayList<HomeworkModel>> doneCallback = result -> {
        homeworkList = result;
        homeworkList = HeaderItemDecoration.makeHeaders(homeworkList, activity.getString(R.string.helper_homeworkUnavailable), HomeworkModel.class);
        homeworkViewAdapter.setSourceList(homeworkList);
    };

    private void postFromDataSet() {
        activity.services.dataService.getFilteredHomework(postDataset).done(doneCallback);
    }

    public void sectionInit() {
        //Setup HTTP POST data
        postDataset.date1.add(Calendar.DAY_OF_MONTH,1);
        postDataset.date2.add(Calendar.DAY_OF_MONTH,1);
        postDataset.allHW = false;
        postDataset.subjectID = "";

        doneCallback.onDone(activity.services.dataService.getCachedHomework());
        activity.services.dataService.getNewHomework().done(result -> {
            homeworkList = result.first;
            homeworkList = HeaderItemDecoration.makeHeaders(homeworkList, activity.getString(R.string.helper_homeworkUnavailable), HomeworkModel.class);
            homeworkViewAdapter.setSourceList(homeworkList);
            subjects = result.second;
            makeMenuItem();
        });

        activity.recyclerView.setAdapter(homeworkViewAdapter);
        headerItemDecoration = new HeaderItemDecoration(homeworkViewAdapter, activity.recyclerView);
        activity.recyclerView.addItemDecoration(headerItemDecoration);
        activity.showView(activity.recyclerView);
    }

    public void sectionClose() {
        activity.hideView(activity.recyclerView);
        activity.recyclerView.removeItemDecoration(headerItemDecoration);
        activity.actionBarMenu.removeItem(1);
        homeworkList.clear();
    }

    private void makeMenuItem() {
        activity.actionBarMenu.add(Menu.NONE, 1, Menu.NONE, R.string.dialog_homeworkTitle)
                .setIcon(R.drawable.ic_search)
                .setOnMenuItemClickListener(menuItem -> {
                    makeDialog();
                    return false;
                }
                ).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    private void makeDialog() {
        tempPostDataset = postDataset; //initialize temporary data set
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.dialog_homework, null);
        builder.setView(dialogView);
        builder.setTitle(R.string.dialog_homeworkTitle);
        builder.setPositiveButton(R.string.button_filterIt, (dialogInterface, i) -> {
            postDataset = tempPostDataset;
            postFromDataSet();
        });
        builder.setNegativeButton(R.string.button_cancel, (dialogInterface, i) -> {
        });

        int subjectSelection = 0;
        for(NameIdPair subject : subjects)if(tempPostDataset.subjectID.equals(subject.id))subjectSelection = subjects.indexOf(subject);

        subjectListAdapter subjectListAdapter = new subjectListAdapter();
        datePickerDialog = builder.create();
        datePickerDialog.show();
        TextView dateFromText = datePickerDialog.findViewById(R.id.homeworkDialog_dateFrom);
        dateFromText.setText(activity.getString(R.string.dialog_homeworkDateFrom, dateFormat.format(postDataset.date1.getTime())));
        TextView dateToText = datePickerDialog.findViewById(R.id.homeworkDialog_dateTo);
        dateToText.setText(activity.getString(R.string.dialog_homeworkDateTo, dateFormat.format(postDataset.date2.getTime())));
        Spinner subjectDropDown = datePickerDialog.findViewById(R.id.homeworkDialog_homeworkFilter);
        subjectDropDown.setAdapter(subjectListAdapter);
        subjectDropDown.setSelection(subjectSelection);

        dateFromText.setOnClickListener(view -> {
            DatePickerFragment fragment = new DatePickerFragment();
            fragment.setDataSet(tempPostDataset.date1, Homework.this);
            datePickerMode = 0;
            fragment.show(activity.getFragmentManager(), activity.getString(R.string.dialog_homeworkDatePickerFrom));
        });

        dateToText.setOnClickListener(view -> {
            DatePickerFragment fragment = new DatePickerFragment();
            fragment.setDataSet(tempPostDataset.date2, Homework.this);
            datePickerMode = 1;
            fragment.show(activity.getFragmentManager(), activity.getString(R.string.dialog_homeworkDatePickerTo));
        });

        subjectDropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tempPostDataset.subjectID = subjects.get(i).id;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        //Set data in struct
        switch (datePickerMode) {
            case 0: tempPostDataset.date1.set(year, month, day); break;
            case 1: tempPostDataset.date2.set(year, month, day); break;
        }

        TextView dateFromText = datePickerDialog.findViewById(R.id.homeworkDialog_dateFrom);
        dateFromText.setText(activity.getString(R.string.dialog_homeworkDateFrom, dateFormat.format(tempPostDataset.date1.getTime())));
        TextView dateToText = datePickerDialog.findViewById(R.id.homeworkDialog_dateTo);
        dateToText.setText(activity.getString(R.string.dialog_homeworkDateTo, dateFormat.format(tempPostDataset.date2.getTime())));

    }

}
