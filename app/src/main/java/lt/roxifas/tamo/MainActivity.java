package lt.roxifas.tamo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import lt.roxifas.tamo.models.dataItems.PremadeDialogModel;
import lt.roxifas.tamo.models.viewHolders.AccountHolder;
import lt.roxifas.tamo.sections.About;
import lt.roxifas.tamo.sections.DevTools;
import lt.roxifas.tamo.sections.Holidays;
import lt.roxifas.tamo.sections.Notes;
import lt.roxifas.tamo.sections.Tests;
import lt.roxifas.tamo.sections.Timetable;
import lt.roxifas.tamo.utils.CommonRecyclerAdapter;
import lt.roxifas.tamo.utils.OneTimeDialogLauncher;
import lt.roxifas.tamo.utils.Utils;
import lt.roxifas.tamo.models.dataItems.AccountModel;
import lt.roxifas.tamo.models.SectionModel;
import lt.roxifas.tamo.sections.Gradetable;
import lt.roxifas.tamo.sections.Homework;
import lt.roxifas.tamo.sections.Login;
import lt.roxifas.tamo.sections.Messages;
import lt.roxifas.tamo.sections.Semesters;
import lt.roxifas.tamo.services.ServiceBlock;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public Spinner actionBarDropDown;
    public ArrayAdapter<String> actionBarDropDownAdapter;
    public Menu actionBarMenu;
    public RelativeLayout mainLayout;
    public ServiceBlock services;
    public RecyclerView recyclerView;
    public StatusBarHandler statusBarHandler;

    private Utils utils = Utils.getInstance();
    public NavigationView navigationView;
    private Toolbar toolbar;
    public DrawerLayout drawer;
    private DrawerClosedListener drawerListener = new DrawerClosedListener();
    private int lastChoiceID = -1;
    final private int fadeAnimationDuration = 200;
    private MenuItem messageItem;
    private TextView userNameText;
    final public RelativeLayout.LayoutParams contentParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
    private ArrayList<SectionModel> sections = new ArrayList<>();
    private Login loginSection;
    private AlertDialog accountsDialog;
    private final int loginSectionId = -2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Utils.setupInstance(this);
        //Launch second initialization thread
        utils.runInMainThread(() -> {
            //Setup toolbar
            toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            //Setup navigationView
            navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(MainActivity.this);

            //Initialize all references to views
            drawer = findViewById(R.id.drawer_layout);
            drawer.addDrawerListener(drawerListener);
            actionBarDropDown = findViewById(R.id.toolbarSpinner);
            mainLayout = findViewById(R.id.content_main);
            userNameText = navigationView.getHeaderView(0).findViewById(R.id.navigationDrawer_userName);
            userNameText.setOnClickListener((view) -> makeAccountDialog());
            //Setup public recyclerView
            recyclerView = new RecyclerView(MainActivity.this);
            recyclerView.setLayoutParams(contentParams);
            recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            recyclerView.setVisibility(View.GONE);
            mainLayout.addView(recyclerView);
            //Setup the action bar
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(MainActivity.this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            statusBarHandler = new StatusBarHandler(MainActivity.this);
            if(services != null) {
                services.setStatusBarHandler(statusBarHandler);
            }
        });
        new InitializationThread().start();
        //Setup toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        showDialogs();
        createNotificationChannel();
    }

    @Override
    public void setTitle(int titleId) {
        toolbar.setTitle(titleId);
    }

    @Override
    public void setTitle(CharSequence title) {
        toolbar.setTitle(title);
    }

    @Override
    public void onBackPressed() {
        if(lastChoiceID >= 0) {
            if(!sections.get(lastChoiceID).onBackButton()) {
                return;
            }
        }
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id == lastChoiceID)return false; //Do not recreate already active section
        //Cleanup before changing active section
        drawerListener.sectionId = id;
        drawerListener.runSectionSwitch = true;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        actionBarMenu = menu;
        return true;
    }

    public void addView(final View view) {
        AlphaAnimation fadeInAnimation = new AlphaAnimation(0, 1);
        fadeInAnimation.setDuration(fadeAnimationDuration);
        mainLayout.addView(view);
        view.setVisibility(View.VISIBLE);
        view.startAnimation(fadeInAnimation);
    }

    public void removeView(final View view) {
        AlphaAnimation fadeOutAnimation = new AlphaAnimation(1, 0);
        fadeOutAnimation.setDuration(fadeAnimationDuration);

        utils.runInMainThreadDelayed(() -> mainLayout.removeView(view), fadeAnimationDuration);
        view.startAnimation(fadeOutAnimation);
    }

    public void hideView(final View view) {
        AlphaAnimation fadeOutAnimation = new AlphaAnimation(1, 0);
        fadeOutAnimation.setDuration(fadeAnimationDuration);
        fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        view.startAnimation(fadeOutAnimation);
    }

    public void showView(final View view) {
        AlphaAnimation fadeInAnimation = new AlphaAnimation(0, 1);
        fadeInAnimation.setDuration(fadeAnimationDuration);
        fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {}

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        view.startAnimation(fadeInAnimation);
    }

    public void setUnreadMessageCount(int count) {
        if(count != 0) messageItem.setTitle(getString(R.string.sectionName_MessagesWithCount, count));
        else messageItem.setTitle(R.string.sectionName_Messages);
    }

    private void populateSectionMenu() {
        for(int i = 0 ; i < sections.size() ; i++) {
            navigationView.getMenu().add(sections.get(i).getGroup(), i, Menu.NONE, sections.get(i).getName())
                    .setIcon(sections.get(i).getIcon());
            if(sections.get(i) instanceof Messages) {
                messageItem = navigationView.getMenu().findItem(i);
            }
        }
        navigationView.getMenu().setGroupCheckable(SectionModel.sectionGroup, true, true);
        navigationView.getMenu().setGroupCheckable(SectionModel.settingsGroup, true, true);
    }

    private void onActiveAccountChange(long activeId) {
        if(activeId == AccountModel.invalidId) {
            navigationView.getMenu().setGroupEnabled(SectionModel.sectionGroup, false);
            launchLoginSection();
            userNameText.setText(R.string.notLoggedIn);
            accountsDialog.cancel();
            drawer.closeDrawer(GravityCompat.START);
            return;
        }
        AccountModel account = services.accountsService.getActiveAccount();
        utils.runInMainThread(() -> {
            userNameText.setText(account.getFullname());
            if(accountsDialog != null) {
                accountsDialog.cancel();
            }
            drawer.openDrawer(GravityCompat.START);
            closeActiveSection();
            navigationView.getMenu().setGroupEnabled(SectionModel.sectionGroup, true);
            String toastText = getString(R.string.login_hint_loggedInAsName, account.getFullname());
            Toast.makeText(MainActivity.this, toastText, Toast.LENGTH_LONG).show();
        });
    }

    //For asynchronously running heavy parts of the initialization process
    private class InitializationThread extends Thread {
        @Override
        public void run() {
            //Setup global classes
            services = new ServiceBlock(MainActivity.this);
            services.accountsService.activeId.onChange(MainActivity.this::onActiveAccountChange);
            if(statusBarHandler != null) {
                services.setStatusBarHandler(statusBarHandler);
            }

            //Create all sections ahead of time
            sections.add(new Homework(MainActivity.this));
            sections.add(new Gradetable(MainActivity.this));
            sections.add(new Semesters(MainActivity.this));
            sections.add(new Messages(MainActivity.this));
            sections.add(new Timetable(MainActivity.this));
            sections.add(new Holidays(MainActivity.this));
            sections.add(new Notes(MainActivity.this));
            sections.add(new Tests(MainActivity.this));
            sections.add(new About(MainActivity.this));
            //noinspection ConstantConditions
            if(BuildConfig.BUILD_TYPE.equals("debug")) {
                sections.add(new DevTools(MainActivity.this));
            }
            utils.runInMainThread(() -> {
                populateSectionMenu();
                navigationView.getMenu()
                        .add(SectionModel.settingsGroup, Menu.NONE, Menu.NONE, getString(R.string.helper_buildInfo, BuildConfig.VERSION_NAME))
                        .setIcon(R.drawable.ic_codetags)
                        .setEnabled(false);
            });

            AccountModel account = services.accountsService.getActiveAccount();
            if(account == null) {
                utils.runInMainThread(() -> {
                    launchLoginSection();
                    navigationView.getMenu().setGroupEnabled(SectionModel.sectionGroup, false);
                });
            } else {
                utils.runInMainThread(() -> {
                    userNameText.setText(account.getFullname());
                    drawer.openDrawer(GravityCompat.START);
                });
            }
        }
    }

    class DrawerClosedListener implements DrawerLayout.DrawerListener {
        boolean runSectionSwitch = false;
        int sectionId;
        @Override
        public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

        }

        @Override
        public void onDrawerOpened(@NonNull View drawerView) {

        }

        @Override
        public void onDrawerClosed(@NonNull View drawerView) {
            if(runSectionSwitch) {
                if(lastChoiceID >= 0) {
                    sections.get(lastChoiceID).sectionClose();
                } else if (lastChoiceID == loginSectionId) {
                    loginSection.sectionClose();
                }
                sections.get(sectionId).sectionInit();
                setTitle(sections.get(sectionId).getName());
                lastChoiceID = sectionId;
                runSectionSwitch = false;
            }
        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void showDialogs() {
        utils.runInMainThread(() -> {
            OneTimeDialogLauncher dialogLauncher = new OneTimeDialogLauncher(this);
            PremadeDialogModel dialog = new PremadeDialogModel(this);
            dialog.setTitle(R.string.dialog_userNotice);
            dialog.setContent(R.string.userNotice);
            dialog.enableAgreementRequirement();
            dialogLauncher.addDialog(dialog, 1);
            dialogLauncher.displayDialogs();
        });
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.fileNotificationChannelName);
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(Utils.FileNotificationChannel, name, importance);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }

    private void closeActiveSection() {
        if(lastChoiceID == -1) {
            return;
        }
        if(lastChoiceID > -1) {
            sections.get(lastChoiceID).sectionClose();
            navigationView.getMenu().getItem(lastChoiceID).setChecked(false);
        } else if(lastChoiceID == loginSectionId) {
            loginSection.sectionClose();
        }
        lastChoiceID = -1;
        setTitle(R.string.app_name);
    }

    private void launchLoginSection() {
        if(loginSection == null) {
            loginSection = new Login(this);
        }
        closeActiveSection();
        loginSection.sectionInit();
        lastChoiceID = loginSectionId;
    }

    private void makeAccountDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        ArrayList<AccountModel> list = services.accountsService.accounts.getList();
        if(list.size() == 0) {
            Toast.makeText(this, R.string.info_noSavedAccounts, Toast.LENGTH_SHORT).show();
            return;
        }
        CommonRecyclerAdapter<AccountHolder, AccountModel> adapter = new CommonRecyclerAdapter<>(R.layout.element_account, this, AccountHolder.class);
        adapter.setSourceList(list);
        RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setPadding(utils.dpToPx(16), 0, utils.dpToPx(16), 0);
        builder.setTitle(R.string.dialog_accountList);
        builder.setView(recyclerView);
        builder.setNegativeButton(R.string.button_cancel, null);
        builder.setPositiveButton(R.string.button_addAccount, (dialog, which) -> {
            launchLoginSection();
            drawer.closeDrawer(GravityCompat.START);
        });
        accountsDialog = builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(grantResults.length == 0) {
            utils.permissionCallback(requestCode, PackageManager.PERMISSION_DENIED);
        } else {
            utils.permissionCallback(requestCode, grantResults[0]);
        }
    }

}
