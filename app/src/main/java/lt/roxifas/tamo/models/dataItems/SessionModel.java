package lt.roxifas.tamo.models.dataItems;

import android.util.Pair;

import java.util.Calendar;
import java.util.Date;

import lt.roxifas.tamo.services.AccountsService;

public class SessionModel {
    private long userId;
    private String cookies;
    private Date lastRequestDate;
    private AccountsService accountsService;
    private String username;
    private String password;

    public SessionModel(AccountsService service, Date date, String cookies, long id) {
        accountsService = service;
        lastRequestDate = date;
        this.cookies = cookies;
        userId = id;
        AccountModel account = accountsService.getAccountById(userId);
        username = account.getUsername();
        password = account.getPassword();
    }

    public String getCookies() {
        return cookies;
    }

    public void updateCookies(String cookies) {
        this.cookies = cookies;
        accountsService.updateSessionCookies(userId, cookies);
    }

    public void updateLastTime() {
        lastRequestDate = Calendar.getInstance().getTime();
        accountsService.updateSessionTime(userId, lastRequestDate);
    }

    public boolean isInvalid() {
        Calendar currentTime = Calendar.getInstance();
        currentTime.add(Calendar.MINUTE, -49);

        return lastRequestDate.getTime() <= currentTime.getTimeInMillis();
    }

    public void invalidate() {
        Calendar invalidDate = Calendar.getInstance();
        invalidDate.add(Calendar.MINUTE, -50);
        lastRequestDate = invalidDate.getTime();
        accountsService.updateSessionTime(userId, lastRequestDate);
    }

    //Returns pair of username and password
    public Pair<String, String> getLoginData() {
        return new Pair<>(username, password);
    }

}
