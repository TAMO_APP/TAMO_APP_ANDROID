package lt.roxifas.tamo.models.viewHolders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.TimetableModel;

public class TimetableHolder extends CommonViewHolder<TimetableModel> {
    private TextView topText;
    private TextView subText;
    public TimetableHolder(View itemView, int layoutId) {
        super(itemView, layoutId);
        if(layoutId != R.layout.element_header) {
            topText = itemView.findViewById(R.id.subtextEntry_main);
            subText = itemView.findViewById(R.id.subtextEntry_sub);
        }
    }

    @Override
    public void bindItem(TimetableModel item, Context context) {
        topText.setText(item.name);
        String subString = item.order + ' ' + item.time;
        subText.setText(subString);
    }
}
