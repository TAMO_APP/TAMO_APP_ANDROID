package lt.roxifas.tamo.models.viewHolders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.AboutItemModel;

public class AboutHolder extends CommonViewHolder<AboutItemModel> {
    private TextView textView;
    private ImageView imageView;

    public AboutHolder(View itemView, int layoutId) {
        super(itemView, layoutId);
        textView = itemView.findViewById(R.id.textView);
        imageView = itemView.findViewById(R.id.imageView);
    }

    @Override
    public void bindItem(AboutItemModel item, Context context) {
        textView.setText(item.textResource);
        imageView.setImageDrawable(context.getDrawable(item.imageResource));
        itemView.setOnClickListener(item.listener);
    }
}
