package lt.roxifas.tamo.models.dataItems;

public class TimetableModel extends ListItemModel {
    public String weekday;
    public String order;
    public String time;
    public String name;

    @Override
    public String getHeaderSource() {
        return weekday;
    }
}
