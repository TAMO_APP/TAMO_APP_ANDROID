package lt.roxifas.tamo.models.dataItems;

public class HomeworkModel extends ListItemModel {
    public String date;
    public String subject;
    public String teacher;
    public String homework;
    public String completionDate;
    public String entryDate;

    public String getHeaderSource() {
        return completionDate;
    }
}
