package lt.roxifas.tamo.models;

import android.view.Menu;

import lt.roxifas.tamo.MainActivity;

public abstract class SectionModel {
    //Menu group ids
    public static int sectionGroup = 1;
    public static int settingsGroup = 2;
    //Instance variables
    protected MainActivity activity;
    private int nameId = 0;
    private int iconId = 0;
    private int groupId = Menu.NONE;

    protected void setName(int id) {
        nameId = id;
    }

    protected void setIcon(int id) {
        iconId = id;
    }

    protected void setGroup(int id) {
        groupId = id;
    }

    public int getName() {
        if(nameId == 0) {
            throw new InternalError("The section didn't set its' name");
        }
        return nameId;
    }

    public int getIcon() {
        if(iconId == 0) {
            throw new InternalError("The section didn't set its' icon");
        }
        return iconId;
    }

    public int getGroup() {
        return groupId;
    }

    public SectionModel(MainActivity _activity) {
        activity = _activity;
    }

    public abstract void sectionInit();
    public abstract void sectionClose();
    public boolean onBackButton() {
        return true;
    }

}
