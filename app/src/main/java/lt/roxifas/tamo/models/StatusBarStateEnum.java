package lt.roxifas.tamo.models;

public enum StatusBarStateEnum {
    hidden,
    loading,
    error
}
