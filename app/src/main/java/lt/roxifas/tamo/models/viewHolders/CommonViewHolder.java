package lt.roxifas.tamo.models.viewHolders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.ListItemModel;

public abstract class CommonViewHolder<T extends ListItemModel> extends RecyclerView.ViewHolder {
    private TextView headerText;

    CommonViewHolder(View itemView, int layoutId) {
        super(itemView);
        if(layoutId == R.layout.element_header) {
            headerText = itemView.findViewById(R.id.header_text);
        }
    }

    public void setHeaderText(String text) {
        headerText.setText(text);
    }

    public abstract void bindItem(T item, Context context);

}
