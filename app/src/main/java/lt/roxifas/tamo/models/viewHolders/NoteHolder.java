package lt.roxifas.tamo.models.viewHolders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.NoteModel;

public class NoteHolder extends CommonViewHolder<NoteModel> {
    private TextView entryDateText;
    private TextView lessonDateText;
    private TextView entryText;
    public NoteHolder(View itemView, int layoutId) {
        super(itemView, layoutId);
        entryDateText = itemView.findViewById(R.id.notes_entryDate);
        lessonDateText = itemView.findViewById(R.id.notes_lessonDate);
        entryText = itemView.findViewById(R.id.notes_entryText);
    }

    @Override
    public void bindItem(NoteModel item, Context context) {
        entryDateText.setText(context.getString(R.string.helper_entryDateAndTeacher, item.teachersName, item.entryDate));
        lessonDateText.setText(context.getString(R.string.helper_dateOfLesson, item.lessonDate));
        entryText.setText(item.entryText);
    }
}
