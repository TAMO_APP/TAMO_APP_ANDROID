package lt.roxifas.tamo.models.dataItems;

public abstract class ListItemModel {
    public boolean isHeader = false;
    public int headerPosition;
    public String headerText;
    public abstract String getHeaderSource();
}
