package lt.roxifas.tamo.models.dataItems;

import android.support.annotation.NonNull;

import lt.roxifas.tamo.R;

public class TestModel extends ListItemModel implements Comparable<TestModel> {
    public TestType type;
    public String subject;
    public String date;

    @Override
    public int compareTo(@NonNull TestModel comparable) {
        return date.compareTo(comparable.date);
    }

    @Override
    public String getHeaderSource() {
        return date;
    }

    public enum TestType {//Type acronyms taken from tamo.lt
        L, K, D, A, S, T, PR, RA, PD, TD, unknown;

        public static TestType fromString(String input) {
            switch (input) {
                case "L": return TestType.L;
                case "K": return TestType.K;
                case "D": return TestType.D;
                case "A": return TestType.A;
                case "S": return TestType.S;
                case "T": return TestType.T;
                case "PR": return TestType.PR;
                case "RA": return TestType.RA;
                case "PD": return TestType.PD;
                case "TD": return TestType.TD;
                default: return TestType.unknown;
            }
        }

        public String toString() {
            switch (this) {
                case L: return "L";
                case K: return "K";
                case D: return "D";
                case A: return "A";
                case S: return "S";
                case T: return "T";
                case PR: return "PR";
                case RA: return "RA";
                case PD: return "PD";
                case TD: return "TD";
                default: return "?";
            }
        }

        public int toLabel() {
            switch (this) {
                case L: return R.string.testType_L;
                case K: return R.string.testType_K;
                case D: return R.string.testType_D;
                case A: return R.string.testType_A;
                case S: return R.string.testType_S;
                case T: return R.string.testType_T;
                case PR: return R.string.testType_PR;
                case RA: return R.string.testType_RA;
                case PD: return R.string.testType_PD;
                case TD: return R.string.testType_TD;
                default: return R.string.testType_unknown;
            }
        }
    }

}
