package lt.roxifas.tamo.models.viewHolders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.TestModel;

public class TestHolder extends CommonViewHolder<TestModel> {
    private TextView label;
    private TextView typeLabel;
    public TestHolder(View itemView, int layoutId) {
        super(itemView, layoutId);
        if(layoutId != R.layout.element_header) {
            label = itemView.findViewById(R.id.subtextEntry_main);
            typeLabel = itemView.findViewById(R.id.subtextEntry_sub);
        }
    }

    @Override
    public void bindItem(TestModel item, Context context) {
        label.setText(item.subject);
        typeLabel.setText(context.getString(item.type.toLabel()));
    }
}
