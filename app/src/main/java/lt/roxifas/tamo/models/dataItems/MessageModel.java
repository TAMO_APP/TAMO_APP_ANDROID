package lt.roxifas.tamo.models.dataItems;

import java.util.ArrayList;

public class MessageModel {
    public int attachmentCount;
    public String text;
    public ArrayList<String> attachmentNames = new ArrayList<>();
    public ArrayList<String> attachmentId = new ArrayList<>();
    public int listingPosition;
}
