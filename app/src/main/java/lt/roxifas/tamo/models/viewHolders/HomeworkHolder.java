package lt.roxifas.tamo.models.viewHolders;

import android.content.Context;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.HomeworkModel;
import lt.roxifas.tamo.utils.Utils;

public class HomeworkHolder extends CommonViewHolder<HomeworkModel> {
    private TextView textView;
    private TextView subjectText;
    private TextView dateText;
    private TextView entryDateText;
    public HomeworkHolder(View itemView, int viewType) {
        super(itemView, viewType);
        if(viewType != R.layout.element_header) {
            textView = itemView.findViewById(R.id.homework_mainText);
            subjectText = itemView.findViewById(R.id.homework_subjectName);
            dateText = itemView.findViewById(R.id.homework_lessonDate);
            entryDateText = itemView.findViewById(R.id.homework_teacherAndEntry);
        }
    }

    @Override
    public void bindItem(HomeworkModel item, Context context) {
        textView.setText(item.homework);
        subjectText.setText(item.subject);
        dateText.setText(item.date);
        String entryString = context.getString(R.string.helper_entryDateAndTeacher, item.teacher, item.entryDate);
        entryDateText.setText(entryString);
        Linkify.addLinks(textView, Utils.linkPattern, "");
    }
}
