package lt.roxifas.tamo.models.dataItems;

public class NameIdPair {
    public String name;
    public String id;
    public boolean selected = false;
}
