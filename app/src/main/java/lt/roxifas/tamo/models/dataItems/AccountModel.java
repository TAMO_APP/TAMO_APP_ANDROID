package lt.roxifas.tamo.models.dataItems;

import lt.roxifas.tamo.services.AccountsService;

public class AccountModel extends ListItemModel {
    public final static long invalidId = -1;
    private String username;
    private String password;
    private String fullname;
    private AccountType accountType;
    private AccountsService service;

    private long id;

    public AccountModel(long id, AccountsService service, String username, String password, String fullname, AccountType type) {
        this.id = id;
        this.service = service;
        this.username = username;
        this.password = password;
        this.fullname = fullname;
        this.accountType = type;
    }

    public long getId() {
        return id;
    }

    @Override
    public String getHeaderSource() {
        return null;
    }

    public enum AccountType {
        Student,
        Parent,
        Teacher,
        Unset
    }

    public void delete() {
        if(service == null) {
            throw new IllegalStateException("Service was not given during construction");
        }
        service.accounts.remove(this);
    }

    public boolean isActive() {
        if(service == null) {
            throw new IllegalStateException("Service was not given during construction");
        }
        return service.activeId.get() == id;
    }

    public void makeActive() {
        if(service == null) {
            throw new IllegalStateException("Service was not given during construction");
        }
        service.activeId.set(id);
    }

    public String getFullname() {
        return fullname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public AccountType getType() {
        return accountType;
    }

}
