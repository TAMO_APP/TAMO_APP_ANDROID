package lt.roxifas.tamo.models.viewHolders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.HolidayModel;

public class HolidayHolder extends CommonViewHolder<HolidayModel> {
    private TextView title;
    private TextView beginDate;
    private TextView endDate;
    public HolidayHolder(View itemView, int layoutId) {
        super(itemView, layoutId);
        title = itemView.findViewById(R.id.holiday_title);
        beginDate = itemView.findViewById(R.id.holiday_beginDate);
        endDate = itemView.findViewById(R.id.holiday_endDate);
    }

    @Override
    public void bindItem(HolidayModel item, Context context) {
        title.setText(item.name);
        beginDate.setText(context.getString(R.string.helper_holidayStart, item.beginDate));
        endDate.setText(context.getString(R.string.helper_holidayEnd, item.endDate));
    }
}
