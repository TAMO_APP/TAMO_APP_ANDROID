package lt.roxifas.tamo.models.dataItems;

public class MessageListItemModel {
    public boolean isRead;
    public boolean hasAttachment;
    public String identificator;
    public String title;
    public String date;
    public String sender;
    public boolean selected = false;
}
