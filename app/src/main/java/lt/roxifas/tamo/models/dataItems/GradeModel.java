package lt.roxifas.tamo.models.dataItems;

import android.content.Context;
import android.support.annotation.NonNull;

import lt.roxifas.tamo.R;

public class GradeModel extends ListItemModel implements Comparable<GradeModel> {
    public enum GradeType {
        practicalTask,
        controlTask,
        simpleTask,
        soloTask,
        homeTask,
        classTask,
        theoreticalTask,
        creditGrade,
        test,
        cumulativeGrade,
        otherInstitutions,
        project,
        essay,
        unknown,
        attendance_n,
        attendance_nt,
        attendance_nv,
        attendance_np,
        attendance_nl,
        attendance_nk,
        attendance_ns,
        attendance_ng,
        attendance_p
    }

    public String grade;
    public String subject;
    public String date;
    public GradeType type;

    public String getMainText() {
        return grade + " - " + subject;
    }

    public String getTypeText(Context context) {
        switch (type) {
            case practicalTask: return context.getString(R.string.gradeType_practicalTask);
            case controlTask: return context.getString(R.string.gradeType_controlTask);
            case simpleTask: return context.getString(R.string.gradeType_simpleTask);
            case soloTask: return context.getString(R.string.gradeType_soloTask);
            case homeTask: return context.getString(R.string.gradeType_homeTask);
            case classTask: return context.getString(R.string.gradeType_classTask);
            case theoreticalTask: return context.getString(R.string.gradeType_theoreticalTask);
            case creditGrade: return context.getString(R.string.gradeType_creditGrade);
            case test: return context.getString(R.string.gradeType_test);
            case cumulativeGrade: return context.getString(R.string.gradeType_cumulativeGrade);
            case otherInstitutions: return context.getString(R.string.gradeType_otherInstitutions);
            case project: return context.getString(R.string.gradeType_project);
            case essay: return context.getString(R.string.gradeType_essay);
            case unknown: return context.getString(R.string.gradeType_unknown);
            case attendance_n: return context.getString(R.string.gradeType_attendance_n);
            case attendance_nt: return context.getString(R.string.gradeType_attendance_nt);
            case attendance_nv: return context.getString(R.string.gradeType_attendance_nv);
            case attendance_np: return context.getString(R.string.gradeType_attendance_np);
            case attendance_nl: return context.getString(R.string.gradeType_attendance_nl);
            case attendance_nk: return context.getString(R.string.gradeType_attendance_nk);
            case attendance_ns: return context.getString(R.string.gradeType_attendance_ns);
            case attendance_ng: return context.getString(R.string.gradeType_attendance_ng);
            case attendance_p: return context.getString(R.string.gradeType_attendance_p);
            default: return context.getString(R.string.gradeType_unknown);
        }
    }
    
    @Override
    public int compareTo(@NonNull GradeModel comparable) {
        return date.compareTo(comparable.date);
    }

    @Override
    public String getHeaderSource() {
        return date;
    }
}