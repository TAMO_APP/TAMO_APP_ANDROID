package lt.roxifas.tamo.models.viewHolders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.DevToolItem;

public class DevToolHolder extends CommonViewHolder<DevToolItem> {
    private TextView textView;

    public DevToolHolder(View itemView, int layoutId) {
        super(itemView, layoutId);
        textView = itemView.findViewById(R.id.textView);
    }

    @Override
    public void bindItem(DevToolItem item, Context context) {
        textView.setText(item.textResource);
        itemView.setOnClickListener(item.listener);
    }
}
