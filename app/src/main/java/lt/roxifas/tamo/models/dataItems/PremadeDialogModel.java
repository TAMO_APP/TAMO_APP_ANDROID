package lt.roxifas.tamo.models.dataItems;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import org.jdeferred2.Deferred;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;

public class PremadeDialogModel {
    private String title;
    private String content;
    private Drawable icon;
    private boolean agreementRequired = false;
    private Context context;

    public PremadeDialogModel(Context context) {
        this.context = context;
    }

    public void setTitle(@StringRes int titleId) {
        title = context.getString(titleId);
    }

    public void setContent(@StringRes int contentId) {
        content = context.getString(contentId);
    }

    public void setIcon(@DrawableRes int iconId) {
        icon = context.getDrawable(iconId);
    }

    public void enableAgreementRequirement() {
        agreementRequired = true;
    }

    public Promise<Void, Void, Void> show() {
        Deferred<Void, Void, Void> deferred = new DeferredObject<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        if(title != null) {
            builder.setTitle(title);
        }
        if(content != null) {
            builder.setMessage(content);
        }
        if(icon != null) {
            builder.setIcon(icon);
        }
        if(agreementRequired) {
            builder.setPositiveButton(android.R.string.yes, (dialog, which) -> deferred.resolve(null));
            builder.setNegativeButton(android.R.string.no, (dialog, which) -> deferred.reject(null));
        } else {
            builder.setPositiveButton(android.R.string.ok, (dialog, which) -> deferred.resolve(null));
        }
        builder.show();
        return deferred.promise();
    }
}
