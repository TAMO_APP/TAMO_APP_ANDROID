package lt.roxifas.tamo.models.viewHolders;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.AccountModel;

public class AccountHolder extends CommonViewHolder<AccountModel> {
    private TextView nameText;
    private ImageView deleteButton;
    public AccountHolder(View itemView, int layoutId) {
        super(itemView, layoutId);
        nameText = itemView.findViewById(R.id.textView);
        deleteButton = itemView.findViewById(R.id.imageView);
    }

    @Override
    public void bindItem(AccountModel item, Context context) {
        nameText.setText(item.getFullname());
        if(item.isActive()) {
            nameText.setTypeface(null, Typeface.BOLD);
        } else {
            nameText.setTypeface(null, Typeface.NORMAL);
        }
        nameText.setOnClickListener((view) -> {
            if(!item.isActive()) {
                item.makeActive();
            } else {
                Toast.makeText(context, R.string.into_accountAlreadyActive, Toast.LENGTH_SHORT).show();
            }
        });
        deleteButton.setOnClickListener((view) -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(context.getString(R.string.helper_accountDeleteConfirmation, item.getFullname()));
            builder.setPositiveButton(R.string.button_ok, (dialog, which) -> item.delete());
            builder.setNegativeButton(R.string.button_cancel, null);
            builder.show();
        });

    }
}
