package lt.roxifas.tamo.models;

public interface ServiceSubscriber {
    void notify(Object data);
}
