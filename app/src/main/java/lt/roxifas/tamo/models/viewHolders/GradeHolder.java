package lt.roxifas.tamo.models.viewHolders;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.GradeModel;

public class GradeHolder extends CommonViewHolder<GradeModel> {
    private TextView textView;
    private TextView typeView;
    public GradeHolder(View itemView, int layoutId) {
        super(itemView, layoutId);
        if(layoutId != R.layout.element_header) {
            textView = itemView.findViewById(R.id.grade_main);
            typeView = itemView.findViewById(R.id.grade_explain);
        }
    }

    @Override
    public void bindItem(GradeModel item, Context context) {
        textView.setText(item.getMainText());
        typeView.setText(item.getTypeText(context));
    }
}
