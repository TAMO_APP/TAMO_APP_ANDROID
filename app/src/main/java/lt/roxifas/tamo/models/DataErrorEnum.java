package lt.roxifas.tamo.models;

public enum DataErrorEnum {
    none,
    noNetwork,
    networkFail,
    notLoggedIn,
    parsing,
    cache
}
