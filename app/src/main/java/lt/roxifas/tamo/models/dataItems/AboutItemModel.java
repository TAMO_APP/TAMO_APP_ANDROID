package lt.roxifas.tamo.models.dataItems;

import android.view.View;

public class AboutItemModel extends ListItemModel {
    public int textResource;
    public int imageResource;
    public View.OnClickListener listener;

    @Override
    public String getHeaderSource() {
        return null;
    }
}
