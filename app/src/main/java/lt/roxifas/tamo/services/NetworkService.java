package lt.roxifas.tamo.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Pair;
import android.webkit.MimeTypeMap;

import org.jdeferred2.Deferred;
import org.jdeferred2.DoneCallback;
import org.jdeferred2.FailCallback;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.LinkedBlockingDeque;

import javax.net.ssl.HttpsURLConnection;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.StatusBarHandler;
import lt.roxifas.tamo.models.DataErrorEnum;
import lt.roxifas.tamo.utils.Utils;
import lt.roxifas.tamo.models.dataItems.SessionModel;
import lt.roxifas.tamo.models.StatusBarStateEnum;

class NetworkService {
    private Promise<TamoAuth.LoginResult, DataErrorEnum, Void> loginPromise;
    private SessionModel currentSession;
    private TamoAuth auth;
    private boolean threadRunning = true;
    private LinkedBlockingDeque<NetworkRequest> requestQueue = new LinkedBlockingDeque<>(10);
    private ConnectivityManager cm;
    private StatusBarHandler statusBarHandler;
    private NotificationManager notificationManager;
    private Context context;
    public enum RequestTypes {
        get,
        post,
        getFile
    }

    private void textRequest(NetworkRequest request) {
        try {
            URL urlObject = new URL(request.address);
            HttpsURLConnection connection = (HttpsURLConnection) urlObject.openConnection();
            connection.setRequestProperty("Cookie", currentSession.getCookies());

            if(request.type == RequestTypes.post) {
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(request.requestData);
                writer.flush();
                writer.close();
            }
            connection.connect();

            InputStream inputStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            String data;
            StringBuilder stringBuilder = new StringBuilder();
            while ((data = reader.readLine()) != null) {
                stringBuilder.append(data).append('\n');
            }
            request.result = stringBuilder.toString();


        } catch (IOException e) {
            e.printStackTrace();
            request.error = DataErrorEnum.networkFail;
            request.result = Utils.getStackTrace(e);
        }
    }

    private void fileRequest(NetworkRequest request) {
        final String downloadsDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
        notificationManager.cancel(1);
        Notification.Builder notificationBuilder = Utils.getInstance().createNotificationBuilder(context, Utils.FileNotificationChannel);
        notificationBuilder.setContentTitle(context.getString(R.string.notification_attachmentDownloadProgress))
                .setContentText(request.requestData)
                .setSmallIcon(android.R.drawable.stat_sys_download);
        notificationBuilder.setProgress(0, 0, true);
        notificationManager.notify(1, notificationBuilder.build());

        try {
            URL urlObject = new URL(request.address);
            HttpsURLConnection connection = (HttpsURLConnection) urlObject.openConnection();
            connection.setRequestProperty("Cookie", currentSession.getCookies());
            connection.connect();

            //Open input stream from the HTTPS connection
            InputStream inputStream = connection.getInputStream();

            //Open output stream to save into file
            String saveFilePath = downloadsDirectory + File.separator + request.requestData;
            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead;
            byte[] buffer = new byte[512];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.close();
            inputStream.close();
            } catch (IOException e) {
                notificationBuilder = Utils.getInstance().createNotificationBuilder(context, Utils.FileNotificationChannel);
                notificationBuilder.setContentTitle(context.getString(R.string.notification_attachmentDownloadFailed))
                        .setProgress(0, 0, false)
                        .setSmallIcon(android.R.drawable.stat_notify_error);
                notificationManager.notify(1, notificationBuilder.build());
                e.printStackTrace();
                request.error = DataErrorEnum.networkFail;
                request.result = Utils.getStackTrace(e);
                return;
            }
        String filePath = downloadsDirectory + File.separator + request.requestData;
        Intent notificationIntent = new Intent(Intent.ACTION_VIEW);
        Uri fileUri = FileProvider.getUriForFile(context, context.getPackageName()+".fileprovider", new File(filePath));
        notificationIntent.setDataAndType(fileUri, MimeTypeMap.getSingleton().getMimeTypeFromExtension(filePath.substring(filePath.lastIndexOf('.')+1)));
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                .addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        notificationBuilder = Utils.getInstance().createNotificationBuilder(context, Utils.FileNotificationChannel);
        notificationBuilder.setContentTitle(context.getString(R.string.notification_attachmentDownloadDone))
                .setContentText(request.requestData)
                .setProgress(0, 0, false)
                .setSmallIcon(android.R.drawable.stat_sys_download_done)
                .setAutoCancel(true)
                .setContentIntent(intent);
        notificationManager.notify(1, notificationBuilder.build());
    }

    private class NetworkThread extends Thread {
        public void run() {
            NetworkRequest request;
            while(true) {
                try {
                    request = requestQueue.takeFirst();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    threadRunning = false;
                    break;
                }

                if(request.updateSessionTime)currentSession.updateLastTime();
                switch (request.type) {
                    case get: //Purposeful fallthrough
                    case post: textRequest(request); break;
                    case getFile: fileRequest(request); break;
                }
                if(request.error == DataErrorEnum.none) {
                    hideStatusIfNoRequests();
                    request.deferred.resolve(request.result);
                } else {
                    statusError();
                    request.deferred.reject(request.error);
                }
            }
        }
    }

    private void putCurrentSessionInEffect() {
        if(currentSession == null) return;
        if(currentSession.isInvalid()) {
            Pair<String, String> pair = currentSession.getLoginData();
            loginPromise = auth.login(pair.first, pair.second);
            loginPromise.done(loginDoneHandler).fail(loginFailedHandler);
            statusLoggingIn();
        }
    }

    NetworkService(ServiceBlock serviceBlock) {
        context = serviceBlock.context;
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        auth = serviceBlock.auth;
        cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkThread thread = new NetworkThread();
        thread.start();
        currentSession = serviceBlock.accountsService.activeSession.get();
        putCurrentSessionInEffect();
        serviceBlock.accountsService.activeSession.onChange((value -> {
            currentSession = value;
            putCurrentSessionInEffect();
        }));
    }

    void setStatusBarHandler(StatusBarHandler statusBarHandler) {
        this.statusBarHandler = statusBarHandler;
    }

    //Public accessor functions
    Promise<String, DataErrorEnum, Void> get(String inputURL) {
        Deferred<String, DataErrorEnum, Void> deferred = new DeferredObject<>();
        Utils.runInBackgroundThread(() -> {
            NetworkRequest request = new NetworkRequest(inputURL, RequestTypes.get, deferred);
            addRequestToQueue(request);
        });
        return deferred.promise();
    }

    Promise<String, DataErrorEnum, Void> post(String inputURL, String postString) {
        Deferred<String, DataErrorEnum, Void> deferred = new DeferredObject<>();
        Utils.runInBackgroundThread(() -> {
            NetworkRequest request = new NetworkRequest(inputURL, RequestTypes.post, deferred);
            request.requestData = postString;
            addRequestToQueue(request);
        });
        return deferred.promise();
    }

    Promise<String, DataErrorEnum, Void> getFile(String inputURL, String filename) {
        Deferred<String, DataErrorEnum, Void> deferred = new DeferredObject<>();
        NetworkRequest request = new NetworkRequest(inputURL, RequestTypes.getFile, deferred);
        Utils.runInBackgroundThread(() -> {
            request.requestData = filename;
            addRequestToQueue(request);
        });
        return deferred.promise();
    }

    private boolean networkUnavailable() {
        //Check if network is available
        if(!threadRunning)new NetworkThread().start();
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean unavailable = activeNetwork == null || !activeNetwork.isConnected();
        if(unavailable) {
            if(statusBarHandler != null) {
                statusBarHandler.setStatus(StatusBarStateEnum.error, R.string.error_noNetwork);
            }
        }
        return unavailable;
    }

    private void addRequestToQueue(NetworkRequest request) {
        if(networkUnavailable()) {
            request.deferred.reject(DataErrorEnum.noNetwork);
            return;
        }
        if(currentSession == null) {
            throw new IllegalStateException("A request has been placed without an active session");
        }
        if(currentSession.isInvalid()) {
            if(loginPromise == null) {
                Pair<String, String> pair = currentSession.getLoginData();
                loginPromise = auth.login(pair.first, pair.second);
                loginPromise.done(loginDoneHandler).fail(loginFailedHandler);
                statusLoggingIn();
            }
            loginPromise.done((result) -> requestQueue.addLast(request)).fail((result) -> request.deferred.reject(DataErrorEnum.notLoggedIn));
        } else {
            statusLoading();
            requestQueue.addLast(request);
        }
    }

    private void statusLoading() {
        if(statusBarHandler != null) {
            statusBarHandler.setStatus(StatusBarStateEnum.loading, R.string.info_loading);
        }
    }
    private void statusLoggingIn() {
        if(statusBarHandler != null) {
            statusBarHandler.setStatus(StatusBarStateEnum.loading, R.string.info_loggingIn);
        }
    }
    private void statusError() {
        if(statusBarHandler != null) {
            statusBarHandler.setStatus(StatusBarStateEnum.error, R.string.error_networkFailure);
        }
    }
    private void statusLoginError() {
        if(statusBarHandler != null) {
            statusBarHandler.setStatus(StatusBarStateEnum.error, R.string.loginFailed_generic);
        }
    }
    private void hideStatusIfNoRequests() {
        if(requestQueue.isEmpty() && statusBarHandler != null) {
            statusBarHandler.setStatus(StatusBarStateEnum.hidden, 0);
        }
    }

    private DoneCallback<TamoAuth.LoginResult> loginDoneHandler = result -> {
        currentSession.updateLastTime();
        currentSession.updateCookies(result.cookies);
        if(requestQueue.size() > 0) {
            statusLoading();
        }
        loginPromise = null;
    };

    private FailCallback<DataErrorEnum> loginFailedHandler = result -> {
        currentSession.invalidate();
        requestQueue.clear();
        statusLoginError();
        loginPromise = null;
    };

    private class NetworkRequest {
        NetworkRequest(String address, RequestTypes type, Deferred<String, DataErrorEnum, Void> deferred) {
            this.address = address;
            this.type = type;
            this.deferred = deferred;
        }

        String address;
        String requestData = "";
        DataErrorEnum error = DataErrorEnum.none;
        RequestTypes type;
        String result = "";
        boolean updateSessionTime = true;
        Deferred<String, DataErrorEnum, Void> deferred;
    }
}
