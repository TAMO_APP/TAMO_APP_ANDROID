package lt.roxifas.tamo.services;

import android.content.Context;

import lt.roxifas.tamo.StatusBarHandler;

public class ServiceBlock {
    Context context;
    public AccountsService accountsService;
    CachedDataService cachedDataService;
    LiveDataService liveDataService;
    public DataService dataService;
    NetworkService networkService;
    ParsingService parsingService;
    TamoAuth auth;

    public void setStatusBarHandler(StatusBarHandler input) {
        networkService.setStatusBarHandler(input);
    }

    public ServiceBlock(Context context) {
        this.context = context;
        auth = new TamoAuth(context);
        accountsService = new AccountsService(this);
        parsingService = new ParsingService();
        networkService = new NetworkService(this);
        cachedDataService = new CachedDataService(this);
        liveDataService = new LiveDataService(this);
        dataService = new DataService(this);
    }

    public void deleteCurrentCachedData() {
        cachedDataService.clearCacheTables();
        dataService.getAllCachedLists();
    }

}
