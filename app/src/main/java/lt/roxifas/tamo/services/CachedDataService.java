package lt.roxifas.tamo.services;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.jdeferred2.Deferred;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;

import java.util.ArrayList;

import lt.roxifas.tamo.models.dataItems.HolidayModel;
import lt.roxifas.tamo.models.dataItems.NoteModel;
import lt.roxifas.tamo.models.dataItems.TestModel;
import lt.roxifas.tamo.models.dataItems.TimetableModel;
import lt.roxifas.tamo.utils.BiMap;
import lt.roxifas.tamo.utils.DataServiceDeferred;
import lt.roxifas.tamo.utils.DataServicePromise;
import lt.roxifas.tamo.utils.Utils;
import lt.roxifas.tamo.models.dataItems.AccountModel;
import lt.roxifas.tamo.models.dataItems.GradeModel;
import lt.roxifas.tamo.models.dataItems.HomeworkModel;
import lt.roxifas.tamo.models.dataItems.MessageListItemModel;
import lt.roxifas.tamo.models.dataItems.SemesterModel;

class CachedDataService {

    private SQLiteDatabase cacheDatabase;
    private SQLiteDatabase writableCacheDatabase;
    private long activeId;

    private BiMap<Integer, GradeModel.GradeType> gradetypeMap = new BiMap<>();
    private BiMap<Integer, Boolean> intBoolMap = new BiMap<>();

    CachedDataService(ServiceBlock serviceBlock) {
        CacheHelper cacheHelper = new CacheHelper(serviceBlock.context);
        cacheDatabase = cacheHelper.getReadableDatabase();
        writableCacheDatabase = cacheHelper.getWritableDatabase();

        gradetypeMap.put(0, GradeModel.GradeType.practicalTask);
        gradetypeMap.put(1, GradeModel.GradeType.controlTask);
        gradetypeMap.put(2, GradeModel.GradeType.simpleTask);
        gradetypeMap.put(3, GradeModel.GradeType.soloTask);
        gradetypeMap.put(4, GradeModel.GradeType.homeTask);
        gradetypeMap.put(5, GradeModel.GradeType.classTask);
        gradetypeMap.put(6, GradeModel.GradeType.theoreticalTask);
        gradetypeMap.put(7, GradeModel.GradeType.creditGrade);
        gradetypeMap.put(8, GradeModel.GradeType.test);
        gradetypeMap.put(9, GradeModel.GradeType.cumulativeGrade);
        gradetypeMap.put(10, GradeModel.GradeType.otherInstitutions);
        gradetypeMap.put(11, GradeModel.GradeType.project);
        gradetypeMap.put(12, GradeModel.GradeType.essay);
        gradetypeMap.put(13, GradeModel.GradeType.unknown);
        gradetypeMap.put(14, GradeModel.GradeType.attendance_n);
        gradetypeMap.put(15, GradeModel.GradeType.attendance_nt);
        gradetypeMap.put(16, GradeModel.GradeType.attendance_nv);
        gradetypeMap.put(17, GradeModel.GradeType.attendance_np);
        gradetypeMap.put(18, GradeModel.GradeType.attendance_nl);
        gradetypeMap.put(19, GradeModel.GradeType.attendance_nk);
        gradetypeMap.put(20, GradeModel.GradeType.attendance_ns);
        gradetypeMap.put(21, GradeModel.GradeType.attendance_ng);
        gradetypeMap.put(22, GradeModel.GradeType.attendance_p);

        intBoolMap.put(0, false);
        intBoolMap.put(1, true);
        serviceBlock.accountsService.accounts.onRemoved((value -> clearCacheTables(value.getId())));
        onNewActiveId(serviceBlock.accountsService.activeId.get());
        serviceBlock.accountsService.activeId.onChange(CachedDataService.this::onNewActiveId);
    }

    private void onNewActiveId(Long id) {
        activeId = id;
        if(activeId != AccountModel.invalidId) {
            ensureTablesAvailable();
        }
    }

    private interface TableEntryGetter<T> {
        T get(Cursor cursor);
    }

    private <T> void getList(DataServiceDeferred<ArrayList<T>> deferred, String tableName, TableEntryGetter<T> getter) {
        Utils.runInBackgroundThread(() -> {
            if(activeId == AccountModel.invalidId)return;
            ArrayList<T> list = new ArrayList<>();
            Cursor cacheCursor = cacheDatabase.rawQuery("SELECT * FROM " + tableName + "_" + activeId, null);
            list.ensureCapacity(cacheCursor.getCount());
            cacheCursor.moveToFirst();
            for(int i = 0 ; i < cacheCursor.getCount() ; i++) {
                list.add(getter.get(cacheCursor));
                cacheCursor.moveToNext();
            }
            cacheCursor.close();
            deferred.resolve(list);
        });
    }

    DataServicePromise<ArrayList<HomeworkModel>> getHomeworkList() {
        DataServiceDeferred<ArrayList<HomeworkModel>> deferred = new DataServiceDeferred<>();
        getList(deferred, "homework", cursor -> {
            HomeworkModel entry = new HomeworkModel();
            entry.date = cursor.getString(0);
            entry.subject = cursor.getString(1);
            entry.teacher = cursor.getString(2);
            entry.homework = cursor.getString(3);
            entry.completionDate = cursor.getString(4);
            entry.entryDate = cursor.getString(5);
            return entry;
        });
        return deferred.promise();
    }

    void setHomeworkList(ArrayList<HomeworkModel> list) {
        Utils.runInBackgroundThread(() -> {
            if(activeId == AccountModel.invalidId)return;
            writableCacheDatabase.execSQL("DELETE FROM homework_" + activeId);//Wipe previous records
            ContentValues rowToInsert;
            for(int i = 0 ; i < list.size() ; i++) {
                rowToInsert = new ContentValues();
                rowToInsert.put("date", list.get(i).date);
                rowToInsert.put("subject", list.get(i).subject);
                rowToInsert.put("teacher", list.get(i).teacher);
                rowToInsert.put("homework", list.get(i).homework);
                rowToInsert.put("completionDate", list.get(i).completionDate);
                rowToInsert.put("entryDate", list.get(i).entryDate);
                writableCacheDatabase.insert("homework_" + activeId, null, rowToInsert);
            }
        });
    }

    DataServicePromise<ArrayList<GradeModel>> getGradetableList() {
        DataServiceDeferred<ArrayList<GradeModel>> deferred = new DataServiceDeferred<>();
        getList(deferred, "grades", cursor -> {
            GradeModel grade = new GradeModel();
            grade.grade = cursor.getString(0);
            grade.subject = cursor.getString(1);
            grade.date = cursor.getString(2);
            grade.type = gradetypeMap.get(cursor.getInt(3));
            return grade;
        });
        return deferred.promise();
    }

    void setGradetableList(ArrayList<GradeModel> list) {
        Utils.runInBackgroundThread(() -> {
            if(activeId == AccountModel.invalidId)return;
            writableCacheDatabase.execSQL("DELETE FROM grades_" + activeId);
            ContentValues contentValues;
            for(int i = 0 ; i < list.size() ; i++) {
                contentValues = new ContentValues();
                contentValues.put("grade", list.get(i).grade);
                contentValues.put("subject", list.get(i).subject);
                contentValues.put("date", list.get(i).date);
                contentValues.put("type", gradetypeMap.getKey(list.get(i).type));
                writableCacheDatabase.insert("grades_" + activeId, null, contentValues);
            }
        });
    }

    Promise<ArrayList<SemesterModel>, Object, Object> getSemesterList() {
        Deferred<ArrayList<SemesterModel>, Object, Object> deferred = new DeferredObject<>();
        Utils.runInBackgroundThread(() -> {
            if(activeId == AccountModel.invalidId)return;
            ArrayList<SemesterModel> list = new ArrayList<>();
            Cursor cacheCursor = cacheDatabase.rawQuery("SELECT * FROM semesters_" + activeId, null);
            list.ensureCapacity(cacheCursor.getCount());
            cacheCursor.moveToFirst();
            for (int i = 0 ; i < cacheCursor.getCount() ; i++) {
                SemesterModel semester = new SemesterModel();
                semester.subjectAverages = cacheCursor.getString(0);
                semester.grades = cacheCursor.getString(1);
                list.add(semester);
                cacheCursor.moveToNext();
            }
            cacheCursor.close();
            deferred.resolve(list);
        });
        return deferred.promise();
    }

    void setSemesterList(ArrayList<SemesterModel> list) {
        Utils.runInBackgroundThread(() -> {
            if(activeId == AccountModel.invalidId)return;
            writableCacheDatabase.execSQL("DELETE FROM semesters_" + activeId);
            ContentValues contentValues;
            for(int i = 0 ; i < list.size() ; i++) {
                contentValues = new ContentValues();
                contentValues.put("subject", list.get(i).subjectAverages);
                contentValues.put("grades", list.get(i).grades);
                writableCacheDatabase.insert("semesters_" + activeId, null, contentValues);
            }
        });
    }

    Promise<ArrayList<MessageListItemModel>, Object, Object> getMessageList() {
        Deferred<ArrayList<MessageListItemModel>, Object, Object> deferred = new DeferredObject<>();
        Utils.runInBackgroundThread(() -> {
            if(activeId == AccountModel.invalidId)return;
            ArrayList<MessageListItemModel> list = new ArrayList<>();
            Cursor cacheCursor = cacheDatabase.rawQuery("SELECT * FROM messages_" + activeId, null);
            int messageCount = cacheCursor.getCount();
            cacheCursor.moveToFirst();
            for(int i = 0 ; i < messageCount ; i++) {
                MessageListItemModel message = new MessageListItemModel();
                message.isRead = intBoolMap.get(cacheCursor.getInt(4));
                message.date = cacheCursor.getString(2);
                message.hasAttachment = intBoolMap.get(cacheCursor.getInt(5));
                message.identificator = cacheCursor.getString(3);
                message.sender = cacheCursor.getString(1);
                message.title = cacheCursor.getString(0);
                list.add(message);
                cacheCursor.moveToNext();
            }
            cacheCursor.close();
            deferred.resolve(list);
        });
        return deferred.promise();
    }

    void setMessageList(ArrayList<MessageListItemModel> list) {
        Utils.runInBackgroundThread(() -> {
            writableCacheDatabase.execSQL("DELETE FROM messages_" + activeId);//Wipe previous records
            ContentValues rowToInsert;
            for(MessageListItemModel message : list) {
                rowToInsert = new ContentValues();
                rowToInsert.put("name", message.title);
                rowToInsert.put("sender", message.sender);
                rowToInsert.put("date", message.date);
                rowToInsert.put("id", message.identificator);
                rowToInsert.put("isRead", intBoolMap.getKey(message.isRead));
                rowToInsert.put("hasAttachment", intBoolMap.getKey(message.hasAttachment));
                writableCacheDatabase.insert("messages_" + activeId, null, rowToInsert);
            }
        });

    }

    Promise<ArrayList<TimetableModel>, Object, Object> getTimetable() {
        Deferred<ArrayList<TimetableModel>, Object, Object> deferred = new DeferredObject<>();
        Utils.runInBackgroundThread(() -> {
            if(activeId == AccountModel.invalidId)return;
            Cursor cacheCursor = cacheDatabase.rawQuery("SELECT * FROM timetable_" + activeId, null);
            ArrayList<TimetableModel> list = new ArrayList<>();
            cacheCursor.moveToFirst();
            for(int i = 0 ; i < cacheCursor.getCount() ; i++) {
                TimetableModel entry = new TimetableModel();
                entry.order = cacheCursor.getString(0);
                entry.time = cacheCursor.getString(1);
                entry.name = cacheCursor.getString(2);
                entry.weekday = cacheCursor.getString(3);
                list.add(entry);
                cacheCursor.moveToNext();
            }
            cacheCursor.close();
            deferred.resolve(list);
        });
        return deferred.promise();
    }

    void setTimetable(ArrayList<TimetableModel> list) {
        Utils.runInBackgroundThread(() -> {
            writableCacheDatabase.execSQL("DELETE FROM timetable_" + activeId);
            ContentValues contentValues;
            for(int i = 0 ; i < list.size() ; i++) {
                contentValues = new ContentValues();
                contentValues.put("orderText", list.get(i).order);
                contentValues.put("time", list.get(i).time);
                contentValues.put("name", list.get(i).name);
                contentValues.put("weekday", list.get(i).weekday);
                writableCacheDatabase.insert("timetable_" + activeId, null, contentValues);
            }
        });
    }

    Promise<ArrayList<HolidayModel>, Object, Object> getHolidays() {
        Deferred<ArrayList<HolidayModel>, Object, Object> deferred = new DeferredObject<>();
        Utils.runInBackgroundThread(() -> {
            if(activeId == AccountModel.invalidId)return;
            Cursor cacheCursor = cacheDatabase.rawQuery("SELECT * FROM holidays_" + activeId, null);
            ArrayList<HolidayModel> list = new ArrayList<>();
            cacheCursor.moveToFirst();
            for(int i = 0 ; i < cacheCursor.getCount() ; i++) {
                HolidayModel holiday = new HolidayModel();
                holiday.name = cacheCursor.getString(0);
                holiday.beginDate = cacheCursor.getString(1);
                holiday.endDate = cacheCursor.getString(2);
                list.add(holiday);
                cacheCursor.moveToNext();
            }
            cacheCursor.close();
            deferred.resolve(list);
        });
        return deferred.promise();
    }

    void setHolidays(ArrayList<HolidayModel> list) {
        Utils.runInBackgroundThread(() -> {
            writableCacheDatabase.execSQL("DELETE FROM holidays_" + activeId);
            ContentValues contentValues;
            for(HolidayModel holiday : list) {
                contentValues = new ContentValues();
                contentValues.put("name", holiday.name);
                contentValues.put("beginDate", holiday.beginDate);
                contentValues.put("endDate", holiday.endDate);
                writableCacheDatabase.insert("holidays_" + activeId, null, contentValues);
            }
        });

    }

    Promise<ArrayList<NoteModel>, Object, Object> getNotes() {
        Deferred<ArrayList<NoteModel>, Object, Object> deferred = new DeferredObject<>();
        Utils.runInBackgroundThread(() -> {
            if(activeId == AccountModel.invalidId)return;
            Cursor cacheCursor = cacheDatabase.rawQuery("SELECT * FROM notes_" + activeId, null);
            ArrayList<NoteModel> list = new ArrayList<>();
            cacheCursor.moveToFirst();
            NoteModel entry;
            for(int i = 0 ; i < cacheCursor.getCount() ; i++) {
                entry = new NoteModel();
                entry.entryDate = cacheCursor.getString(0);
                entry.teachersName = cacheCursor.getString(1);
                entry.lessonDate = cacheCursor.getString(2);
                entry.entryText = cacheCursor.getString(3);
                list.add(entry);
                cacheCursor.moveToNext();
            }
            cacheCursor.close();
            deferred.resolve(list);
        });
        return deferred.promise();
    }

    void setNotes(ArrayList<NoteModel> list) {
        Utils.runInBackgroundThread(() -> {
            writableCacheDatabase.execSQL("DELETE FROM notes_" + activeId);//Wipe previous records
            ContentValues rowToInsert;
            for (int i = 0; i < list.size() ; i++) {
                rowToInsert = new ContentValues();
                rowToInsert.put("entryDate", list.get(i).entryDate);
                rowToInsert.put("teachersName", list.get(i).teachersName);
                rowToInsert.put("lessonDate", list.get(i).lessonDate);
                rowToInsert.put("entryText", list.get(i).entryText);
                writableCacheDatabase.insert("notes_" + activeId, null, rowToInsert);
            }
        });
    }

    Promise<ArrayList<TestModel>, Object, Object> getTests() {
        Deferred<ArrayList<TestModel>, Object, Object> deferred = new DeferredObject<>();
        Utils.runInBackgroundThread(() -> {
            if(activeId == AccountModel.invalidId)return;
            Cursor cacheCursor = cacheDatabase.rawQuery("SELECT * FROM tests_" + activeId, null);
            ArrayList<TestModel> list = new ArrayList<>();
            cacheCursor.moveToFirst();
            while(!cacheCursor.isAfterLast()) {
                TestModel testModel = new TestModel();
                testModel.subject = cacheCursor.getString(0);
                testModel.date = cacheCursor.getString(1);
                testModel.type = TestModel.TestType.fromString(cacheCursor.getString(2));
                list.add(testModel);
                cacheCursor.moveToNext();
            }
            cacheCursor.close();
            deferred.resolve(list);
        });
        return deferred.promise();
    }

    void setTests(ArrayList<TestModel> list) {
        Utils.runInBackgroundThread(() -> {
            writableCacheDatabase.execSQL("DELETE FROM tests_" + activeId);//Wipe previous records
            ContentValues rowToInsert;
            for(int i = 0 ; i < list.size() ; i++) {
                rowToInsert = new ContentValues();
                rowToInsert.put("label", list.get(i).subject);
                rowToInsert.put("date", list.get(i).date);
                rowToInsert.put("type", list.get(i).type.toString());
                cacheDatabase.insert("tests_" + activeId, null, rowToInsert);
            }
        });
    }

    private void ensureTablesAvailable() {
        writableCacheDatabase.execSQL("CREATE TABLE IF NOT EXISTS homework_" + activeId + "(date TEXT, subject TEXT, teacher TEXT, homework TEXT, completionDate TEXT, entryDate TEXT);");
        writableCacheDatabase.execSQL("CREATE TABLE IF NOT EXISTS messages_" + activeId + "(name TEXT, sender TEXT, date TEXT, id TEXT, isRead INTEGER, hasAttachment INTEGER);");
        writableCacheDatabase.execSQL("CREATE TABLE IF NOT EXISTS grades_" + activeId + "(grade TEXT, subject TEXT, date TEXT, type INTEGER);");
        writableCacheDatabase.execSQL("CREATE TABLE IF NOT EXISTS holidays_" + activeId + "(name TEXT, beginDate TEXT, endDate TEXT);");
        writableCacheDatabase.execSQL("CREATE TABLE IF NOT EXISTS semesters_" + activeId + "(subject TEXT, grades TEXT);");
        writableCacheDatabase.execSQL("CREATE TABLE IF NOT EXISTS notes_" + activeId + "(entryDate TEXT, teachersName TEXT, lessonDate TEXT, entryText TEXT);");
        writableCacheDatabase.execSQL("CREATE TABLE IF NOT EXISTS gradeComparison_" + activeId + "(grades REAL, gradeCounts INTEGER);");
        writableCacheDatabase.execSQL("CREATE TABLE IF NOT EXISTS tests_" + activeId + "(label TEXT, date TEXT, type TEXT);");
        writableCacheDatabase.execSQL("CREATE TABLE IF NOT EXISTS timetable_" + activeId + "(orderText TEXT, time TEXT, name TEXT, weekday TEXT);");
    }

    void clearCacheTables() {
        clearCacheTables(activeId);
    }

    void markMessageAsRead(String id) {
        final String readMark = Integer.toString(intBoolMap.getKey(true));
        writableCacheDatabase.execSQL("UPDATE messages_" + activeId + " SET isRead=? WHERE id=?", new String[] {readMark, id});
    }

    void deleteMessage(String id) {
        writableCacheDatabase.execSQL("DELETE FROM messages_" + activeId + " WHERE id=?", new String[] {id});
    }

    private void clearCacheTables(long deletedId) {
        writableCacheDatabase.execSQL("DELETE FROM homework_" + deletedId);
        writableCacheDatabase.execSQL("DELETE FROM messages_" + deletedId);
        writableCacheDatabase.execSQL("DELETE FROM grades_" + deletedId);
        writableCacheDatabase.execSQL("DELETE FROM holidays_" + deletedId);
        writableCacheDatabase.execSQL("DELETE FROM semesters_" + deletedId);
        writableCacheDatabase.execSQL("DELETE FROM notes_" + deletedId);
        writableCacheDatabase.execSQL("DELETE FROM gradeComparison_" + deletedId);
        writableCacheDatabase.execSQL("DELETE FROM tests_" + deletedId);
        writableCacheDatabase.execSQL("DELETE FROM timetable_" + deletedId);
    }

    private class CacheHelper extends SQLiteOpenHelper {
        private static final int databaseVersion = 5;

        CacheHelper(Context context) {
            super(context, "cacheDB", null, databaseVersion);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            switch(oldVersion) {
                case 1: //Purposeful fallthrough
                case 2: //Purposeful fallthrough
                case 3: //Purposeful fallthrough
                case 4: deleteAllTables(db); break;
            }
        }

        private void deleteAllTables(SQLiteDatabase db) {
            ArrayList<String> tables = new ArrayList<>();
            Cursor cursor = db.rawQuery("SELECT * FROM sqlite_master WHERE type='table';", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                String tableName = cursor.getString(1);
                if (!tableName.equals("android_metadata") &&
                        !tableName.equals("sqlite_sequence"))
                    tables.add(tableName);
                cursor.moveToNext();
            }
            cursor.close();

            for(String tableName:tables) {
                db.execSQL("DROP TABLE IF EXISTS " + tableName);
            }
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //In case of downgrade wipe all tables, since their structure could be drastically different
            deleteAllTables(db);
            //Recreate database with current structure
            onCreate(db);
        }
    }

}
