package lt.roxifas.tamo.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import org.jdeferred2.Deferred;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import lt.roxifas.tamo.Addresses;
import lt.roxifas.tamo.models.DataErrorEnum;
import lt.roxifas.tamo.models.dataItems.AccountModel;
import lt.roxifas.tamo.models.dataItems.LoginDataModel;
import lt.roxifas.tamo.utils.Utils;

class TamoAuth {
    private ConnectivityManager cm;
    private CookieManager cookieManager;

    TamoAuth(Context context) {
        cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    private void regenCookieManager() {
        cookieManager = new CookieManager();
        CookieManager.setDefault(cookieManager);
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }

    private String getCookies(HttpsURLConnection connection) {
        //Get cookies
        Map<String, List<String>> headerFields = connection.getHeaderFields();
        List<String> cookiesHeader = headerFields.get("set-Cookie");

        if (cookiesHeader != null) {
            for (String cookie : cookiesHeader) {
                cookieManager.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
            }
        }

        //Save cookies to string
        if (cookieManager.getCookieStore().getCookies().size() > 0) {
            // While joining the Cookies, use ',' or ';' as needed. Most of the servers are using ';'
            return TextUtils.join(";", cookieManager.getCookieStore().getCookies());
        }
        return "";
    }

    private RequestResult networkRequest(String address, String post, String cookies) {
        RequestResult result = new RequestResult();
        try {
            URL urlObject = new URL(address);
            HttpsURLConnection connection = (HttpsURLConnection) urlObject.openConnection();

            if(!cookies.isEmpty()) {
                connection.setRequestProperty("Cookie", cookies);
            }

            if(!post.isEmpty()) {
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(post);
                writer.flush();
                writer.close();
            }
            connection.connect();
            result.httpCode = connection.getResponseCode();
            InputStream inputStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            String data;
            StringBuilder stringBuilder = new StringBuilder();
            while ((data = reader.readLine()) != null) {
                stringBuilder.append(data).append('\n');
            }
            result.html = stringBuilder.toString();
            result.cookies = getCookies(connection);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private RequestResult loginStage1() {
        return networkRequest(Addresses.login, "", "");
    }

    private RequestResult loginStage2(LoginDataModel loginData, String cookies) {
        String postString = loginData.loginString +
                "&IsMobileUser=false&ReturnUrl=&RequireCaptcha=false&Timestamp=" +
                loginData.timestamp + "&SToken=" + loginData.token;

        return networkRequest(Addresses.tamoDomain, postString, cookies);
    }

    Promise<LoginResult, DataErrorEnum, Void> login(String username, String password) {
        Deferred<LoginResult, DataErrorEnum, Void> deferred = new DeferredObject<>();
        Utils.runInBackgroundThread(() -> {
            if(networkUnavailable()) {
                deferred.reject(DataErrorEnum.noNetwork);
                return;
            }
            regenCookieManager();
            String credentialString = "UserName=" + username + "&Password=" + password;
            RequestResult result = loginStage1();
            Document document = Jsoup.parse(result.html);
            LoginDataModel loginData = ParsingService.loginDataset(document);
            if(loginData == null) {
                deferred.reject(DataErrorEnum.parsing);
                return;
            }
            loginData.loginString = credentialString;
            result = loginStage2(loginData, result.cookies);
            document = Jsoup.parse(result.html);
            String name = ParsingService.loginUsername(document);
            if(name.isEmpty()) {
                deferred.reject(DataErrorEnum.notLoggedIn);
                return;
            }
            AccountModel.AccountType type = ParsingService.accountType(document);
            if(type == AccountModel.AccountType.Unset) {
                deferred.reject(DataErrorEnum.parsing);
                return;
            }
            deferred.resolve(new LoginResult(name, type, result.cookies));
        });
        return deferred.promise();
    }

    private boolean networkUnavailable() {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork == null || !activeNetwork.isConnected();
    }

    private class RequestResult {
        String html = "";
        String cookies = "";
        int httpCode = 0;
    }

    class LoginResult {
        String fullname;
        AccountModel.AccountType accountType;
        String cookies;

        LoginResult(String _fullname, AccountModel.AccountType _accountType, String _cookies) {
            fullname = _fullname;
            accountType = _accountType;
            cookies = _cookies;
        }
    }
}
