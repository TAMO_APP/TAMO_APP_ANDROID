package lt.roxifas.tamo.services;

import android.annotation.SuppressLint;
import android.util.Pair;

import org.jdeferred2.Promise;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import lt.roxifas.tamo.Addresses;
import lt.roxifas.tamo.models.DataErrorEnum;
import lt.roxifas.tamo.models.dataItems.FileRequestModel;
import lt.roxifas.tamo.models.dataItems.GradeModel;
import lt.roxifas.tamo.models.dataItems.HolidayModel;
import lt.roxifas.tamo.models.dataItems.HomeworkModel;
import lt.roxifas.tamo.models.dataItems.HomeworkRequestModel;
import lt.roxifas.tamo.models.dataItems.MessageListItemModel;
import lt.roxifas.tamo.models.dataItems.MessageModel;
import lt.roxifas.tamo.models.dataItems.NameIdPair;
import lt.roxifas.tamo.models.dataItems.NoteModel;
import lt.roxifas.tamo.models.dataItems.SemesterModel;
import lt.roxifas.tamo.models.dataItems.TestModel;
import lt.roxifas.tamo.models.dataItems.TimetableModel;
import lt.roxifas.tamo.utils.DataServiceDeferred;
import lt.roxifas.tamo.utils.DataServicePromise;
import lt.roxifas.tamo.utils.Utils;

class LiveDataService {
    private NetworkService networkService;
    private ParsingService parsingService;
    private AccountsService accountsService;
    private TamoAuth auth;

    LiveDataService(ServiceBlock serviceBlock) {
        networkService = serviceBlock.networkService;
        parsingService = serviceBlock.parsingService;
        accountsService = serviceBlock.accountsService;
        auth = serviceBlock.auth;
    }

    private interface ParserProcedure {
        void run(Document document);
    }

    private <T> void defaultHandling(DataServiceDeferred<T> deferred, Promise<String, DataErrorEnum, Void> promise, ParserProcedure parserProcedure) {
        promise.done(result -> {
            Document document = Jsoup.parse(result);
            try {
                parserProcedure.run(document);
            } catch (Exception e) {
                Utils.getInstance().parsingErrorDialog(result, e);
                deferred.reject(DataErrorEnum.parsing);
            }
        }).fail(deferred::reject);
    }

    DataServicePromise<Pair<ArrayList<HomeworkModel>, ArrayList<NameIdPair>>> getHomeworkList() {
        DataServiceDeferred<Pair<ArrayList<HomeworkModel>, ArrayList<NameIdPair>>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.get(Addresses.homework), document ->
                deferred.resolve(new Pair<>(parsingService.homework(document), parsingService.homeworkOptions(document)))
        );
        return deferred.promise();
    }

    DataServicePromise<ArrayList<HomeworkModel>> getHomeworkList(HomeworkRequestModel requestData) {
        String allHomeworkString;
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat postDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateFromString = postDateFormat.format(requestData.date1.getTime());
        String dateToString = postDateFormat.format(requestData.date2.getTime());
        if(requestData.allHW)allHomeworkString = "true"; else allHomeworkString = "false";
        String postString =
                "DalykoId=" + requestData.subjectID +
                "&VisiNd=" + allHomeworkString +
                "&dateFilterMode=0" +
                "&DataNuo=" + dateFromString +
                "&DataIki=" + dateToString;
        DataServiceDeferred<ArrayList<HomeworkModel>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.post(Addresses.homework, postString), document ->
                deferred.resolve(parsingService.homework(document))
        );
        return deferred.promise();
    }

    DataServicePromise<Pair<ArrayList<GradeModel>, ArrayList<String>>> getGradeList() {
        DataServiceDeferred<Pair<ArrayList<GradeModel>, ArrayList<String>>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.get(Addresses.gradetable), document ->
                deferred.resolve(new Pair<>(parsingService.gradetable(document, null), parsingService.gradetableOptions(document)))
        );
        return deferred.promise();
    }

    DataServicePromise<ArrayList<GradeModel>> getGradeList(String option) {
        String[] choiceSplits = option.split("-");
        String postString = "metai=" + choiceSplits[0] + "&menuo=" + choiceSplits[1];
        DataServiceDeferred<ArrayList<GradeModel>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.post(Addresses.gradetableSearch, postString), document ->
                deferred.resolve(parsingService.gradetable(document, option))
        );
        return deferred.promise();
    }

    DataServicePromise<Pair<ArrayList<SemesterModel>, ArrayList<NameIdPair>>> getSemestersList() {
        DataServiceDeferred<Pair<ArrayList<SemesterModel>, ArrayList<NameIdPair>>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.get(Addresses.semesters), document ->
                deferred.resolve(new Pair<>(parsingService.semesters(document), parsingService.semesterOptions(document)))
        );
        return deferred.promise();
    }

    DataServicePromise<ArrayList<SemesterModel>> getSemestersList(String id) {
        DataServiceDeferred<ArrayList<SemesterModel>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.get(Addresses.semesters + id), document ->
                deferred.resolve(parsingService.semesters(document))
            );
        return deferred.promise();
    }

    DataServicePromise<Pair<ArrayList<MessageListItemModel>, Integer>> getMessageList() {
        DataServiceDeferred<Pair<ArrayList<MessageListItemModel>, Integer>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.get(Addresses.messagesList), document ->
                deferred.resolve(new Pair<>(parsingService.messages(document), parsingService.messagePageCount(document)))
            );
        return deferred.promise();
    }

    DataServicePromise<ArrayList<MessageListItemModel>> getMessagePage(int pageNumber) {
        DataServiceDeferred<ArrayList<MessageListItemModel>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.get(Addresses.messagesPage + pageNumber), document ->
                deferred.resolve(parsingService.messages(document))
            );
        return deferred.promise();
    }

    DataServicePromise<MessageModel> getMessage(String id) {
        DataServiceDeferred<MessageModel> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.get(Addresses.messagesText + id), document ->
                deferred.resolve(parsingService.parseMessage(document))
        );
        return deferred.promise();
    }

    void deleteMessage(String identificator) {
        networkService.get(Addresses.messagesDelete + identificator);
    }

    void getFile(FileRequestModel fileRequest) {
        String addressString = Addresses.messagesAttachment + fileRequest.address;
        networkService.getFile(addressString, fileRequest.filename).done((result -> {
        }));
    }

    DataServicePromise<ArrayList<TimetableModel>> getTimetable() {
        DataServiceDeferred<ArrayList<TimetableModel>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.get(Addresses.timetable), document ->
                deferred.resolve(parsingService.timetable(document))
        );
        return deferred.promise();
    }

    DataServicePromise<ArrayList<HolidayModel>> getHolidays() {
        DataServiceDeferred<ArrayList<HolidayModel>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.get(Addresses.holidays), document ->
                deferred.resolve(parsingService.holidays(document))
        );
        return deferred.promise();
    }

    DataServicePromise<ArrayList<NoteModel>> getNotes() {
        DataServiceDeferred<ArrayList<NoteModel>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.get(Addresses.notes), document ->
                deferred.resolve(parsingService.notes(document))
        );
        return deferred.promise();
    }

    DataServicePromise<Pair<ArrayList<TestModel>, ArrayList<NameIdPair>>> getTests() {
        DataServiceDeferred<Pair<ArrayList<TestModel>, ArrayList<NameIdPair>>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.get(Addresses.tests), document ->
                deferred.resolve(new Pair<>(parsingService.tests(document), parsingService.testOptions(document)))
        );
        return deferred.promise();
    }

    DataServicePromise<ArrayList<TestModel>> getTests(String monthId) {
        DataServiceDeferred<ArrayList<TestModel>> deferred = new DataServiceDeferred<>();
        defaultHandling(deferred, networkService.get(Addresses.testsSearch + monthId), document ->
                deferred.resolve(parsingService.tests(document))
        );
        return deferred.promise();
    }

    DataServicePromise<Void> checkNewCredentials(String username, String password) {
        DataServiceDeferred<Void> deferred = new DataServiceDeferred<>();
        auth.login(username, password).done(result -> {
            long id = accountsService.createAccount(username, password, result.fullname, result.accountType, result.cookies);
            accountsService.activeId.set(id);
            deferred.resolve(null);

        }).fail(deferred::reject);
        return deferred.promise();
    }
}
