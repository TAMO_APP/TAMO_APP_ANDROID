package lt.roxifas.tamo.services;

import android.annotation.SuppressLint;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import lt.roxifas.tamo.models.dataItems.AccountModel;
import lt.roxifas.tamo.models.dataItems.GradeModel;
import lt.roxifas.tamo.models.dataItems.HolidayModel;
import lt.roxifas.tamo.models.dataItems.HomeworkModel;
import lt.roxifas.tamo.models.dataItems.LoginDataModel;
import lt.roxifas.tamo.models.dataItems.MessageListItemModel;
import lt.roxifas.tamo.models.dataItems.MessageModel;
import lt.roxifas.tamo.models.dataItems.NameIdPair;
import lt.roxifas.tamo.models.dataItems.NoteModel;
import lt.roxifas.tamo.models.dataItems.SemesterModel;
import lt.roxifas.tamo.models.dataItems.TestModel;
import lt.roxifas.tamo.models.dataItems.TimetableModel;

public class ParsingService {
    private HashMap<String, GradeModel.GradeType> gradeTypeMap = new HashMap<>();

    public ParsingService() {
        gradeTypeMap.put("Praktinis darbas",GradeModel.GradeType.practicalTask);
        gradeTypeMap.put("Kontrolinis darbas",GradeModel.GradeType.controlTask);
        gradeTypeMap.put("Paprastas darbas",GradeModel.GradeType.simpleTask);
        gradeTypeMap.put("Savarankiškas darbas",GradeModel.GradeType.soloTask);
        gradeTypeMap.put("Namų darbas",GradeModel.GradeType.homeTask);
        gradeTypeMap.put("Klasės darbas",GradeModel.GradeType.classTask);
        gradeTypeMap.put("Teorinis darbas",GradeModel.GradeType.theoreticalTask);
        gradeTypeMap.put("įsk.",GradeModel.GradeType.creditGrade);
        gradeTypeMap.put("Testas",GradeModel.GradeType.test);
        gradeTypeMap.put("Kaupiamasis",GradeModel.GradeType.cumulativeGrade);
        gradeTypeMap.put("Iš kitų įstaigų",GradeModel.GradeType.otherInstitutions);
        gradeTypeMap.put("Projektas",GradeModel.GradeType.project);
        gradeTypeMap.put("Rašinys",GradeModel.GradeType.essay);
        gradeTypeMap.put("n",  GradeModel.GradeType.attendance_n);
        gradeTypeMap.put("nt", GradeModel.GradeType.attendance_nt);
        gradeTypeMap.put("nv", GradeModel.GradeType.attendance_nv);
        gradeTypeMap.put("np", GradeModel.GradeType.attendance_np);
        gradeTypeMap.put("nl", GradeModel.GradeType.attendance_nl);
        gradeTypeMap.put("nk", GradeModel.GradeType.attendance_nk);
        gradeTypeMap.put("ns", GradeModel.GradeType.attendance_ns);
        gradeTypeMap.put("ng", GradeModel.GradeType.attendance_ng);
        gradeTypeMap.put("p",  GradeModel.GradeType.attendance_p);
    }

    public ArrayList<HomeworkModel> homework(Document document) {
        ArrayList<HomeworkModel> list = new ArrayList<>();
        HomeworkModel homework = new HomeworkModel();
        String dateString = "";

        Element table = document.getElementsByClass("namu_darbai_content").first();
        Elements entries = table.children();
        entries.remove(0);//First item is used for showing the sorting mode
        for(Element entry : entries) {
            switch (entry.attr("style")) {
                case "margin:25px 0px;"://Date node
                    dateString = entry.child(0).child(0).text();
                    break;
                case "margin-top:10px;margin-bottom:10px;"://First homework node, or divider
                    if(entry.child(0).attr("class").equals("col-md-13 col-md-offset-1"))break;//The node is a divider
                    Element element = entry.child(1);
                    homework = new HomeworkModel();
                    homework.subject = element.child(0).text();
                    element = element.child(1);
                    element.child(0).children().remove(0);
                    homework.date = element.child(0).textNodes().get(0).text();
                    element.child(1).children().remove(0);
                    element.child(1).children().remove(0);
                    homework.teacher = element.child(1).textNodes().get(0).text();
                    homework.teacher = homework.teacher.substring(0, homework.teacher.length()-2);//Remove punctuation
                    homework.entryDate = element.child(1).textNodes().get(1).text();
                    homework.entryDate = homework.entryDate.substring(0, homework.entryDate.length()-1);//Remove space
                    break;
                case ""://Second homework node
                    homework.homework = entry.text();
                    homework.completionDate = dateString;
                    list.add(homework);
                    break;
            }
        }
        return list;
    }

    public ArrayList<NameIdPair> homeworkOptions(Document document) {
        ArrayList<NameIdPair> list = new ArrayList<>();
        Element table = document.getElementById("DalykoId");
        Elements subjects = table.children();
        for(Element subject : subjects) {
            NameIdPair entry = new NameIdPair();
            entry.id = subject.attr("value");
            entry.name = subject.text();
            list.add(entry);
        }
        return list;
    }

    @SuppressLint("DefaultLocale")
    public ArrayList<GradeModel> gradetable(Document document, String date) {
        ArrayList<GradeModel> list = new ArrayList<>();
        String dateString;
        //Responses to different month requests don't contain current date, we use what we already have
        if(date != null) {
            dateString = date;
        } else {
            dateString = document.getElementsByClass("date_active").get(0).text();
        }

        Element table = document.getElementById("scrollable_dienynas");
        table = table.getElementsByTag("tbody").get(0);
        Elements elements = table.children();
        for(Element subjectRow : elements) {
            int dayCounter = 0;
            Elements days = subjectRow.children();
            String subject = days.get(0).text();
            days.remove(0);
            for(Element day : days) {
                dayCounter++;
                if(!day.attr("data-original-title").equals("")) {
                    Document dayContent = Jsoup.parse(day.attr("data-original-title"));
                    Elements dayEntries = dayContent.getElementsByTag("div");
                    dayEntries.remove(0);
                    dayEntries.remove(0);
                    for(Element entry : dayEntries) {
                        if(entry.className().equals("stitle"))continue;//Skip grade type separators
                        GradeModel grade = new GradeModel();
                        String[] entryStrings = entry.text().split(",");
                        if(Character.isDigit(entryStrings[0].charAt(0))) {//Entry is a numeric grade
                            grade.grade = entryStrings[0];
                            String typeDescription = entryStrings[1].split("\\(")[0];
                            typeDescription = typeDescription.substring(1, typeDescription.length()-1);
                            grade.type = gradeTypeMap.get(typeDescription);
                        } else {//Entry is an attendance mark
                            grade.grade = entryStrings[0];
                            grade.type = gradeTypeMap.get(entryStrings[0]);
                        }
                        if(grade.type == null) {//Can't use getOrDefault on HashMap, it requires API 24
                            grade.type = GradeModel.GradeType.unknown;
                        }
                        grade.subject = subject;
                        grade.date = String.format("%s-%02d", dateString, dayCounter);
                        list.add(grade);
                    }
                }
            }
        }
        Collections.sort(list);
        return list;
    }

    public ArrayList<String> gradetableOptions(Document document) {
        ArrayList<String> list = new ArrayList<>();

        Element table = document.getElementsByClass("date_selector").get(0);
        Elements options = table.getElementsByTag("a");
        for(Element option : options) {
            String newOption = option.text();
            list.add(newOption);
        }
        return list;
    }

    private SemesterModel parseNormalSemesterEntry(Element element) {
        SemesterModel semester = new SemesterModel();
        String subject = element.child(0).child(0).text();
        semester.grades = element.child(1).text();
        String averageGrade = element.child(2).text();
        if(!averageGrade.isEmpty()) {
            subject = subject.concat(" - ").concat(averageGrade);
        }
        String finalGrade = element.child(3).text();
        if(!finalGrade.isEmpty()) {
            subject = subject.concat(" - ").concat(finalGrade);
        }
        semester.subjectAverages = subject;
        return semester;
    }

    private SemesterModel parseYearlySemesterEntry(Element element) {
        SemesterModel semester = new SemesterModel();
        String subject = element.child(0).child(0).text();
        String grades = "";
        int gradeCount = element.children().size() - 1;//Not including the final grade node
        for(int i = 1 ; i < gradeCount ; i++) {
            grades = grades.concat(element.child(i).text()).concat(" ");
        }
        semester.grades = grades;
        String finalGrade = element.child(gradeCount).text();
        if(!finalGrade.isEmpty()) {
            subject = subject.concat(" - ").concat(finalGrade);
        }
        semester.subjectAverages = subject;
        return semester;
    }

    public ArrayList<SemesterModel> semesters(Document document) {
        ArrayList<SemesterModel> list = new ArrayList<>();
        Elements tables = document.getElementsByTag("tbody");
        Elements subjects = tables.get(0).children();
        int entryCount = subjects.size() - 1; //Last element is the total average, formatted differently
        boolean yearlyTable = document.getElementsByTag("colgroup").get(0).child(0).hasClass("right_border");

        for(int i = 0 ; i < entryCount ; i++) {
            if(yearlyTable) {
                list.add(parseYearlySemesterEntry(subjects.get(i)));
            } else {
                list.add(parseNormalSemesterEntry(subjects.get(i)));
            }
        }
        return list;
    }

    public ArrayList<NameIdPair> semesterOptions(Document document) {
        ArrayList<NameIdPair> list = new ArrayList<>();
        Element table = document.getElementById("laikotarpis");
        Elements entries = table.children();
        for(Element entry : entries) {
            NameIdPair nameIdPair = new NameIdPair();
            nameIdPair.name = entry.text();
            nameIdPair.id = entry.attr("value");
            nameIdPair.selected = entry.hasAttr("selected");
            list.add(nameIdPair);
        }
        return list;
    }

    ArrayList<MessageListItemModel> messages(Document document) {
        ArrayList<MessageListItemModel> list = new ArrayList<>();
        Element table = document.getElementsByTag("tbody").first();
        Elements messageBlocks = table.getElementsByTag("tr");
        messageBlocks.remove(0); //First <tr> element is actually an advertisement
        for (Element messageBlock : messageBlocks) {
            MessageListItemModel message = new MessageListItemModel();
            Elements elements = messageBlock.getElementsByTag("td").get(1).children();
            message.isRead = !messageBlock.getElementsByTag("td").get(1).hasClass("msg_item_notread");
            message.hasAttachment = !elements.get(0).hasClass("hidden");
            message.identificator = elements.get(1).attr("href").split("/")[3];
            message.title = elements.get(1).text();
            message.sender = messageBlock.getElementsByTag("td").get(2).text();
            message.date = messageBlock.getElementsByTag("td").get(3).text();
            list.add(message);
        }
        return list;
    }

    MessageModel parseMessage(Document document) {
        MessageModel message = new MessageModel();
        Elements attachmentContainers = document.getElementsByClass("msg_filename_container");
        message.attachmentCount = attachmentContainers.size();
        for(Element container : attachmentContainers) {
            message.attachmentNames.add(container.getElementsByTag("span").get(0).text());
            message.attachmentId.add(container.getElementsByTag("span").get(2).child(0).attr("href").split("/")[3]);
        }
        message.text = document.getElementsByClass("text_block").get(0).wholeText();
        return message;
    }

    int messagePageCount(Document document) {
        return document.select("a.pageNumber").size();
    }

    ArrayList<TimetableModel> timetable(Document document) {
        ArrayList<TimetableModel> list = new ArrayList<>();
        Elements weekdayElements = document.getElementById("c_main").children();
        weekdayElements.remove(0);//Removes header element
        weekdayElements.remove(0);//Removes week selector element
        for(Element weekday : weekdayElements) {
            String dayName = weekday.getElementsByTag("thead").get(0).text();
            Elements lessonElements = weekday.getElementsByTag("tbody").get(0).children();
            for(Element lesson : lessonElements) {
                if(lesson.children().size() == 1)break;//Only "No lessons" elements have one child
                TimetableModel timetableEntry = new TimetableModel();
                timetableEntry.weekday = dayName;
                timetableEntry.order = lesson.child(1).text();
                timetableEntry.time = lesson.child(2).text();
                timetableEntry.name = lesson.child(3).text();
                list.add(timetableEntry);
            }
        }
        return list;
    }

    ArrayList<HolidayModel> holidays(Document document) {
        ArrayList<HolidayModel> list = new ArrayList<>();
        Elements elements = document.getElementsByClass("cell tripleCol vBorderBoth hBorderBoth left");
        for (Element element : elements) {
            HolidayModel holiday = new HolidayModel();
            holiday.name = element.child(0).text();
            holiday.beginDate = element.child(1).child(1).text();
            holiday.endDate = element.child(2).child(1).text();
            list.add(holiday);
        }

        return list;
    }

    public ArrayList<NoteModel> notes(Document document) {
        ArrayList<NoteModel> list = new ArrayList<>();
        Element table = document.getElementsByClass("records").get(0);
        Elements elements = table.children();
        elements.remove(elements.size()-1);//Last element is an invisible "No notes available" sign
        for(Element element : elements) {
            Element noteElement = element.child(0);
            NoteModel note = new NoteModel();
            note.entryDate = noteElement.child(3).child(1).text();
            note.teachersName = noteElement.child(2).child(1).text();
            note.lessonDate = noteElement.child(3).child(0).text();
            note.entryText = noteElement.child(1).child(1).text();
            list.add(note);
        }
        return list;
    }

    @SuppressLint("DefaultLocale")
    ArrayList<TestModel> tests(Document document) {
        ArrayList<TestModel> list = new ArrayList<>();
        Element table = document.getElementsByTag("tbody").get(0);
        Elements elements = table.children();
        String dateString = document.getElementsByClass("selected").get(0).text();
        for (Element element : elements) {
            Elements dayElements = element.children();
            String subjectName = dayElements.get(0).getElementsByTag("span").get(1).text();
            dayElements.remove(0);
            for (int j = 0 ; j < dayElements.size() ; j++) {
                Elements testElements = dayElements.get(j).getElementsByTag("span");
                for(Element test : testElements) {
                    TestModel testModel = new TestModel();
                    testModel.date = String.format("%s-%02d", dateString, j+1);
                    testModel.type = TestModel.TestType.valueOf(test.text());
                    testModel.subject = subjectName;
                    list.add(testModel);
                }
            }
        }
        Collections.sort(list);
        return list;
    }

    ArrayList<NameIdPair> testOptions(Document document) {
        ArrayList<NameIdPair> list = new ArrayList<>();
        Elements elements = document.getElementsByClass("academic_months").get(0).children();
        for (Element element : elements) {
            NameIdPair pair = new NameIdPair();
            pair.id = element.attr("href").split("=")[1];
            pair.name = element.text();
            pair.selected = element.hasClass("selected");
            list.add(pair);
        }
        return list;
    }

    static LoginDataModel loginDataset(Document document) {
        Element tokenElement = document.getElementById("SToken");
        if(tokenElement == null)return null;
        LoginDataModel loginData = new LoginDataModel();
        loginData.token = tokenElement.attr("value");
        loginData.timestamp = document.getElementById("Timestamp").attr("value").replace(' ', '+');
        return loginData;
    }

    static String loginUsername(Document document) {
        Elements elements = document.getElementsByAttributeValue("style", "font-size:14px;font-weight:600;");
        if(elements.size() != 1) return "";
        return elements.get(0).text();
    }

    static AccountModel.AccountType accountType(Document document) {
        Element element = document.getElementsByClass("c_select_box_link").get(0);
        String typeString = element.text();
        switch (typeString) {
            case "Mokinys": return AccountModel.AccountType.Student;
            //TODO: other type support
            default: return AccountModel.AccountType.Unset;
        }
    }

}
