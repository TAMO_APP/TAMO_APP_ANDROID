package lt.roxifas.tamo.services;

import android.util.Pair;

import java.util.ArrayList;

import lt.roxifas.tamo.models.dataItems.FileRequestModel;
import lt.roxifas.tamo.models.dataItems.HolidayModel;
import lt.roxifas.tamo.models.dataItems.HomeworkRequestModel;
import lt.roxifas.tamo.models.dataItems.MessageModel;
import lt.roxifas.tamo.models.dataItems.NameIdPair;
import lt.roxifas.tamo.models.dataItems.NoteModel;
import lt.roxifas.tamo.models.dataItems.TestModel;
import lt.roxifas.tamo.models.dataItems.TimetableModel;
import lt.roxifas.tamo.utils.DataServiceDeferred;
import lt.roxifas.tamo.utils.DataServicePromise;
import lt.roxifas.tamo.models.dataItems.GradeModel;
import lt.roxifas.tamo.models.dataItems.HomeworkModel;
import lt.roxifas.tamo.models.dataItems.MessageListItemModel;
import lt.roxifas.tamo.models.dataItems.SemesterModel;

public class DataService {
    private CachedDataService cachedProvider;
    private LiveDataService liveProvider;

    private ArrayList<HomeworkModel> homeworkList;
    private ArrayList<GradeModel> gradetableList;
    private ArrayList<SemesterModel> semesterList;
    private ArrayList<MessageListItemModel> messageList;
    private ArrayList<TimetableModel> timetable;
    private ArrayList<HolidayModel> holidays;
    private ArrayList<NoteModel> notes;
    private ArrayList<TestModel> tests;

    private <T> ArrayList<T> makeCopy(ArrayList<T> list) {
        return new ArrayList<>(list);
    }

    DataService(ServiceBlock serviceBlock) {
        cachedProvider = serviceBlock.cachedDataService;
        liveProvider = serviceBlock.liveDataService;
        getAllCachedLists();
        serviceBlock.accountsService.activeId.onChange((value -> getAllCachedLists()));
    }

    void getAllCachedLists() {
        cachedProvider.getHomeworkList().done(result -> homeworkList = result);
        cachedProvider.getGradetableList().done(result -> gradetableList = result);
        cachedProvider.getSemesterList().done(result -> semesterList = result);
        cachedProvider.getMessageList().done(result -> messageList = result);
        cachedProvider.getTimetable().done(result -> timetable = result);
        cachedProvider.getHolidays().done(result -> holidays = result);
        cachedProvider.getNotes().done(result -> notes = result);
        cachedProvider.getTests().done(result -> tests = result);
    }

    public ArrayList<HomeworkModel> getCachedHomework() {
        return makeCopy(homeworkList);
    }

    public DataServicePromise<Pair<ArrayList<HomeworkModel>, ArrayList<NameIdPair>>> getNewHomework() {
        DataServiceDeferred<Pair<ArrayList<HomeworkModel>, ArrayList<NameIdPair>>> deferred = new DataServiceDeferred<>();
        liveProvider.getHomeworkList().done(result -> {
            homeworkList = makeCopy(result.first);
            deferred.resolveInMain(result);
            cachedProvider.setHomeworkList(homeworkList);
        }).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public DataServicePromise<ArrayList<HomeworkModel>> getFilteredHomework(HomeworkRequestModel request) {
        DataServiceDeferred<ArrayList<HomeworkModel>> deferred = new DataServiceDeferred<>();
        liveProvider.getHomeworkList(request).done(deferred::resolveInMain).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public ArrayList<GradeModel> getCachedGrades() {
        return makeCopy(gradetableList);
    }

    public DataServicePromise<Pair<ArrayList<GradeModel>, ArrayList<String>>> getNewGrades() {
        DataServiceDeferred<Pair<ArrayList<GradeModel>, ArrayList<String>>> deferred = new DataServiceDeferred<>();
        liveProvider.getGradeList().done(result -> {
            gradetableList = makeCopy(result.first);
            deferred.resolveInMain(result);
            cachedProvider.setGradetableList(gradetableList);
        }).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public DataServicePromise<ArrayList<GradeModel>> getFilteredGrades(String filter) {
        DataServiceDeferred<ArrayList<GradeModel>> deferred = new DataServiceDeferred<>();
        liveProvider.getGradeList(filter).done(deferred::resolveInMain).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public ArrayList<SemesterModel> getCachedSemester() {
        return makeCopy(semesterList);
    }

    public DataServicePromise<Pair<ArrayList<SemesterModel>, ArrayList<NameIdPair>>> getNewSemester() {
        DataServiceDeferred<Pair<ArrayList<SemesterModel>, ArrayList<NameIdPair>>> deferred = new DataServiceDeferred<>();
        liveProvider.getSemestersList().done(result -> {
            semesterList = makeCopy(result.first);
            deferred.resolveInMain(result);
            cachedProvider.setSemesterList(semesterList);
        }).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public DataServicePromise<ArrayList<SemesterModel>> getFilteredSemester(String filter) {
        DataServiceDeferred<ArrayList<SemesterModel>> deferred = new DataServiceDeferred<>();
        liveProvider.getSemestersList(filter).done(deferred::resolveInMain).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public ArrayList<MessageListItemModel> getCachedMessages() {
        return makeCopy(messageList);
    }

    public DataServicePromise<Pair<ArrayList<MessageListItemModel>, Integer>> getNewMessages() {
        DataServiceDeferred<Pair<ArrayList<MessageListItemModel>, Integer>> deferred = new DataServiceDeferred<>();
        liveProvider.getMessageList().done(result -> {
            messageList = makeCopy(result.first);
            deferred.resolveInMain(result);
            cachedProvider.setMessageList(messageList);
        }).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public DataServicePromise<ArrayList<MessageListItemModel>> getMessagePage(int page) {
        DataServiceDeferred<ArrayList<MessageListItemModel>> deferred = new DataServiceDeferred<>();
        liveProvider.getMessagePage(page).done(result -> {
            messageList.addAll(result);
            deferred.resolveInMain(result);
            cachedProvider.setMessageList(messageList);
        }).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public DataServicePromise<MessageModel> getMessage(String id) {
        DataServiceDeferred<MessageModel> deferred = new DataServiceDeferred<>();
        liveProvider.getMessage(id).done(deferred::resolveInMain).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public void deleteMessage(String id) {
        liveProvider.deleteMessage(id);
        cachedProvider.deleteMessage(id);
    }

    public void markMessageAsRead(String id) {
        cachedProvider.markMessageAsRead(id);
        cachedProvider.getMessageList().done(result -> messageList = result);
    }

    public void downloadFile(FileRequestModel request) {
        liveProvider.getFile(request);
    }

    public ArrayList<TimetableModel> getCachedTimetable() {
        return makeCopy(timetable);
    }

    public DataServicePromise<ArrayList<TimetableModel>> getNewTimetable() {
        DataServiceDeferred<ArrayList<TimetableModel>> deferred = new DataServiceDeferred<>();
        liveProvider.getTimetable().done(result -> {
            timetable = makeCopy(result);
            deferred.resolveInMain(result);
            cachedProvider.setTimetable(timetable);
        }).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public ArrayList<HolidayModel> getCachedHolidays() {
        return makeCopy(holidays);
    }

    public DataServicePromise<ArrayList<HolidayModel>> getNewHolidays() {
        DataServiceDeferred<ArrayList<HolidayModel>> deferred = new DataServiceDeferred<>();
        liveProvider.getHolidays().done(result -> {
            holidays = makeCopy(result);
            deferred.resolveInMain(result);
            cachedProvider.setHolidays(holidays);
        }).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public ArrayList<NoteModel> getCachedNotes() {
        return makeCopy(notes);
    }

    public DataServicePromise<ArrayList<NoteModel>> getNewNotes() {
        DataServiceDeferred<ArrayList<NoteModel>> deferred = new DataServiceDeferred<>();
        liveProvider.getNotes().done(result -> {
            notes = makeCopy(result);
            deferred.resolveInMain(result);
            cachedProvider.setNotes(notes);
        }).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public ArrayList<TestModel> getCachedTests() {
        return makeCopy(tests);
    }

    public DataServicePromise<Pair<ArrayList<TestModel>, ArrayList<NameIdPair>>> getNewTests() {
        DataServiceDeferred<Pair<ArrayList<TestModel>, ArrayList<NameIdPair>>> deferred = new DataServiceDeferred<>();
        liveProvider.getTests().done(result -> {
            tests = makeCopy(result.first);
            deferred.resolveInMain(result);
            cachedProvider.setTests(tests);
        }).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public DataServicePromise<ArrayList<TestModel>> getFilteredTests(String filter) {
        DataServiceDeferred<ArrayList<TestModel>> deferred = new DataServiceDeferred<>();
        liveProvider.getTests(filter).done(deferred::resolveInMain).fail(deferred::rejectInMain);
        return deferred.promise();
    }

    public DataServicePromise<Void> tryNewAccount(String username, String password) {
        DataServiceDeferred<Void> deferred = new DataServiceDeferred<>();
        liveProvider.checkNewCredentials(username, password).done(deferred::resolveInMain).fail(deferred::rejectInMain);
        return deferred.promise();
    }

}
