package lt.roxifas.tamo.services;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import lt.roxifas.tamo.utils.ArrayListWatchable;
import lt.roxifas.tamo.utils.BiMap;
import lt.roxifas.tamo.utils.Utils;
import lt.roxifas.tamo.models.dataItems.AccountModel;
import lt.roxifas.tamo.models.dataItems.SessionModel;
import lt.roxifas.tamo.utils.Watchable;

public class AccountsService {
    private SQLiteDatabase database;
    private BiMap<AccountModel.AccountType, Integer> accountTypeMap = new BiMap<>();
    private SharedPreferences accountPreferences;
    public Watchable<Long> activeId = new Watchable<>();
    public Watchable<SessionModel> activeSession = new Watchable<>();
    public ArrayListWatchable<AccountModel> accounts = new ArrayListWatchable<>();

    AccountsService(ServiceBlock serviceBlock) {
        AccountsDatabase databaseHelper = new AccountsDatabase(serviceBlock.context);
        database = databaseHelper.getWritableDatabase();

        accountTypeMap.put(AccountModel.AccountType.Unset, 0);
        accountTypeMap.put(AccountModel.AccountType.Student, 1);
        accountTypeMap.put(AccountModel.AccountType.Parent, 2);
        accountTypeMap.put(AccountModel.AccountType.Teacher, 3);

        accountPreferences = serviceBlock.context.getSharedPreferences("accountPreferences", Context.MODE_PRIVATE);
        //Get account list and add list change listeners
        Cursor dbCursor = database.rawQuery("SELECT * FROM accounts", null);
        dbCursor.moveToFirst();
        while(!dbCursor.isAfterLast()) {
            accounts.add(getAccountFromCursor(dbCursor));
            dbCursor.moveToNext();
        }
        dbCursor.close();
        accounts.onRemoved((this::deleteAccount));
        //Get current account and add account change listeners
        activeId.set(accountPreferences.getLong("activeAccount", AccountModel.invalidId));
        if(activeId.get() != AccountModel.invalidId) {
            activeSession.set(getSessionById(activeId.get()));
        }
        activeId.onChange((value -> {
            SharedPreferences.Editor editor = accountPreferences.edit();
            editor.putLong("activeAccount", value);
            editor.apply();
            if(activeId.get() != AccountModel.invalidId) {
                activeSession.set(getSessionById(activeId.get()));
            } else {
                activeSession.set(null);
            }
        }));
    }

    public AccountModel getActiveAccount() {
        if(activeId.get() == AccountModel.invalidId) return null;
        return getAccountById(activeId.get());
    }

    long createAccount(String username, String password, String fullname, AccountModel.AccountType type, String cookies) {
        ContentValues values = new ContentValues();
        values.put("username", username);
        values.put("password", password);
        values.put("fullname", fullname);
        values.put("type", accountTypeMap.get(type));
        long id = database.insert("accounts", null, values);

        values = new ContentValues();
        values.put("userId", id);
        values.put("cookies", cookies);
        values.put("networkDate", Utils.dateTimeFormat.format(Calendar.getInstance().getTime()));
        database.insert("sessions", null, values);

        AccountModel account = new AccountModel(id, this, username, password, fullname, type);
        accounts.add(account);
        return id;
    }

    private void deleteAccount(AccountModel account) {
        String idString = Long.toString(account.getId());
        database.execSQL("DELETE FROM accounts WHERE id=?", new String[] {idString});
        database.execSQL("DELETE FROM sessions WHERE userId=?", new String[] {idString});
        if(account.getId() == activeId.get()) {
            ArrayList<Long> list = getAvailableAccountIds();
            if(list.isEmpty()) {
                activeId.set(AccountModel.invalidId);
            } else {
                activeId.set(list.get(0));
            }
        }
    }

    private AccountModel getAccountFromCursor(Cursor cursor) {
        long id = cursor.getLong(0);
        String username = cursor.getString(1);
        String password = cursor.getString(2);
        String fullname = cursor.getString(3);
        AccountModel.AccountType type = accountTypeMap.getKey(cursor.getInt(4));
        return new AccountModel(id, this, username, password, fullname, type);
    }

    private ArrayList<Long> getAvailableAccountIds() {
        ArrayList<Long> list = new ArrayList<>();
        Cursor dbCursor = database.rawQuery("SELECT id FROM accounts", null);
        list.ensureCapacity(dbCursor.getCount());
        dbCursor.moveToFirst();
        while(!dbCursor.isAfterLast()) {
            list.add(dbCursor.getLong(0));
            dbCursor.moveToNext();
        }
        dbCursor.close();
        return list;
    }

    public AccountModel getAccountById(long id) {
        return accounts.find((value -> value.getId() == id));
    }

    public AccountModel getAccountByUsername(String username) {
        return accounts.find(value -> value.getUsername().equals(username));
    }

    private SessionModel getSessionById(long id) {
        if(id == AccountModel.invalidId) throw new IllegalStateException("Active account is invalid");

        Cursor cursor = database.rawQuery("SELECT * FROM sessions WHERE userId=?", new String[] {Long.toString(id)});
        if(cursor.getCount() == 0) throw new IllegalStateException("No session found for active account");
        if(cursor.getCount() > 1) throw new IllegalStateException("More than one session found for active account");
        cursor.moveToFirst();

        String cookies = cursor.getString(1);
        Date date;
        try {
            date = Utils.dateTimeFormat.parse(cursor.getString(2));
        } catch (ParseException e) {
            e.printStackTrace();
            //This date makes the session invalid and forces a re-authentication
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, -50);
            date = calendar.getTime();
        }

        SessionModel session = new SessionModel(this, date, cookies, id);
        cursor.close();
        return session;
    }

    public void updateSessionTime(long id, Date date) {
        String idString = Long.toString(id);
        String dateString = Utils.dateTimeFormat.format(date);
        database.execSQL("UPDATE sessions SET networkDate=? WHERE userId=?", new String[] {dateString, idString});
    }

    public void updateSessionCookies(long id, String cookies) {
        String idString = Long.toString(id);
        database.execSQL("UPDATE sessions SET cookies=? WHERE userId=?", new String[] {cookies, idString});
    }

    private class AccountsDatabase extends SQLiteOpenHelper {
        private static final int databaseVersion = 1;

        AccountsDatabase(Context context) {
            super(context, "accountsDB", null, databaseVersion);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE accounts(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "username TEXT NOT NULL," +
                    "password TEXT NOT NULL," +
                    "fullname TEXT NOT NULL," +
                    "type INTEGER NOT NULL);"
            );
            db.execSQL("CREATE TABLE sessions(" +
                    "userId INTEGER NOT NULL," +
                    "cookies TEXT NOT NULL," +
                    "networkDate TEXT NOT NULL);"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

}
