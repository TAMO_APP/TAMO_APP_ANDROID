package lt.roxifas.tamo;

public class Addresses {
    //Addresses for tamo.lt platform
    public static final String tamoDomain = "https://dienynas.tamo.lt/";
    public static final String login = tamoDomain + "Prisijungimas/Login";
    public static final String gradetable = tamoDomain + "Pamoka/MokinioDienynas";
    public static final String gradetableSearch = tamoDomain + "Pamoka/MokinioDienynasTable";
    public static final String holidays = tamoDomain + "AtostoguDatos";
    public static final String homework = tamoDomain + "Darbai/NamuDarbai";
    public static final String messagesList = tamoDomain + "Pranesimai";
    public static final String messagesAttachment = tamoDomain + "Pranesimai/Siustis/";
    public static final String messagesPage = tamoDomain + "PranesimaiAjax/Gauti?puslapis=";
    public static final String messagesText = tamoDomain + "PranesimaiAjax/GautasPranesimas/";
    public static final String messagesDelete = tamoDomain + "PranesimaiAjax/TrintiGautaPranesima/";
    public static final String notes = tamoDomain + "Pastabos/Mokiniams";
    public static final String semesters = tamoDomain + "PeriodoVertinimas/MokinioVertinimai/";
    public static final String tests = tamoDomain + "Darbai/Atsiskaitymai";
    public static final String testsSearch = tamoDomain + "Darbai/Atsiskaitymai?MoksloMetuMenesioId=";
    public static final String timetable = tamoDomain + "TvarkarascioIrasas/MokinioTvarkarastis";
    //Email addresses for reports
    public static final String errorReport = "rokas.urbelis@protonmail.com";
}
