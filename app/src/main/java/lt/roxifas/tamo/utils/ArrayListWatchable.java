package lt.roxifas.tamo.utils;

import java.util.ArrayList;

public class ArrayListWatchable<T> {
    private ArrayList<T> list = new ArrayList<>();
    private ArrayList<Watcher<T>> addedWatchers = new ArrayList<>();
    private ArrayList<Watcher<T>> removedWatchers = new ArrayList<>();

    public interface Watcher<T> {
        void onChange(T value);
    }

    public interface Finder<T> {
        boolean isTarget(T value);
    }

    public void onAdded(Watcher<T> watcher) {
        addedWatchers.add(watcher);
    }

    public void onRemoved(Watcher<T> watcher) {
        removedWatchers.add(watcher);
    }

    public void add(T newItem) {
        list.add(newItem);
        for (Watcher<T> watcher : addedWatchers) {
            watcher.onChange(newItem);
        }
    }

    public void remove(T targetItem) {
        list.remove(targetItem);
        for (Watcher<T> watcher : removedWatchers) {
            watcher.onChange(targetItem);
        }
    }

    public ArrayList<T> getList() {
        return new ArrayList<>(list);
    }

    public T find(Finder<T> finder) {
        for (T item : list) {
            if(finder.isTarget(item)) {
                return item;
            }
        }
        return null;
    }
}
