package lt.roxifas.tamo.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.FileProvider;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.widget.Toast;

import org.jdeferred2.Deferred;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.regex.Pattern;

import lt.roxifas.tamo.Addresses;
import lt.roxifas.tamo.MainActivity;
import lt.roxifas.tamo.R;

public class Utils {
    private static Utils instance = new Utils();
    private MainActivity activity;
    private Handler mainThreadHandler;
    private DisplayMetrics displayMetrics;
    private SparseArray<Deferred<Void, Void, Void>> permissionRequests = new SparseArray<>();
    private int permissionRequestCounter = 0;
    public static final String FileNotificationChannel = "FileChannel";
    public static final Pattern linkPattern = Pattern.compile("[a-z]+://[^ \\n]*");

    private Utils() {}

    public static void setupInstance(MainActivity activity) {
        instance.activity = activity;
        instance.displayMetrics = activity.getResources().getDisplayMetrics();
    }

    public static Utils getInstance() {
        return instance;
    }

    public static String getStackTrace(Throwable throwable) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        throwable.printStackTrace(printWriter);
        return result.toString();
    }

    public void parsingErrorDialog(String html, Exception exception) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.exception_parser);
        builder.setMessage(R.string.dialog_errorReport);
        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> {});
        builder.setPositiveButton(android.R.string.ok, (dialog, which) ->
                reportError(exception, html)
        );
        runInMainThread(() -> builder.create().show());
    }

    public static @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static @SuppressLint("SimpleDateFormat") SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public void runInMainThread(Runnable runnable) {
        //Ensure handler is not null
        if(mainThreadHandler == null) mainThreadHandler = new Handler(Looper.getMainLooper());
        mainThreadHandler.post(runnable);
    }

    public void runInMainThreadDelayed(Runnable runnable, long delay) {
        //Ensure handler is not null
        if(mainThreadHandler == null) mainThreadHandler = new Handler(Looper.getMainLooper());
        mainThreadHandler.postDelayed(runnable, delay);
    }

    public static void runInBackgroundThread(Runnable runnable) {
        new Thread(runnable).start();
    }

    public static boolean isRunningInMainThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public static String readInputStream(InputStream stream) {
        Scanner scanner = new Scanner(stream);
        String contents = scanner.useDelimiter("\\A").next();
        scanner.close();
        return contents;
    }

    @SuppressWarnings("deprecation")
    public Notification.Builder createNotificationBuilder(Context context, String channelName) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            return new Notification.Builder(context, channelName);
        } else {
            return new Notification.Builder(context);
        }
    }

    public int dpToPx(int dp) {
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private void reportError(Exception exception, String resourceToAttach) {
        final String downloadsDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
        String filePath = downloadsDirectory + File.separator + "tamoAppDump.html";
        ensurePermissionAvailable(Manifest.permission.WRITE_EXTERNAL_STORAGE).done(result -> {
            try {
                FileOutputStream outputStream = new FileOutputStream(filePath);
                outputStream.write(resourceToAttach.getBytes());
            } catch (IOException e) {
                Toast.makeText(activity, R.string.error_fileWriteFailed, Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            Uri fileUri = FileProvider.getUriForFile(activity, activity.getPackageName()+".fileprovider", new File(filePath));
            composeEmailWithAttachment(new String[]{Addresses.errorReport}, "TamoApp error report", getStackTrace(exception), fileUri);
        }).fail(result ->
                Toast.makeText(activity, R.string.error_noFilePermission, Toast.LENGTH_LONG).show()
        );
    }

    private void composeEmailWithAttachment(String[] addresses, String subject, String text, Uri attachment) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        //intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.putExtra(Intent.EXTRA_STREAM, attachment);
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(Intent.createChooser(intent, activity.getString(R.string.info_chooseEmailApp)));
        } else {
            Toast.makeText(activity, R.string.info_noEmailAppFound, Toast.LENGTH_LONG).show();
        }
    }

    public Promise<Void, Void, Void> ensurePermissionAvailable(String permission) {
        Deferred<Void, Void, Void> deferred = new DeferredObject<>();
        Utils.runInBackgroundThread(() -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(activity.checkSelfPermission(permission) == PackageManager.PERMISSION_DENIED) {
                    activity.requestPermissions(new String[]{permission}, permissionRequestCounter);
                    permissionRequests.put(permissionRequestCounter++, deferred);
                } else {
                    runInMainThread(() -> deferred.resolve(null));
                }
            }
        });
        return deferred.promise();
    }

    public void permissionCallback(int requestCode, int result) {
        if(result == PackageManager.PERMISSION_DENIED) {
            permissionRequests.get(requestCode).reject(null);
        } else {
            permissionRequests.get(requestCode).resolve(null);
        }
        permissionRequests.remove(requestCode);
    }

}
