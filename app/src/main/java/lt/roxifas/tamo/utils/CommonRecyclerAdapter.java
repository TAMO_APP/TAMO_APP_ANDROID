package lt.roxifas.tamo.utils;

import android.app.Activity;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.ListItemModel;
import lt.roxifas.tamo.models.viewHolders.CommonViewHolder;

public class CommonRecyclerAdapter<T extends CommonViewHolder, S extends ListItemModel> extends RecyclerView.Adapter<T> {
    private ArrayList<S> sourceList = new ArrayList<>();
    private final int layoutResourceId;
    private final LayoutInflater inflater;
    private Constructor<T> viewHolderConstructor;
    private Activity activity;

    public CommonRecyclerAdapter(@LayoutRes int layoutId, Activity activity, Class<T> viewHolderClass) {
        layoutResourceId = layoutId;
        this.activity = activity;
        this.inflater = activity.getLayoutInflater();
        try {
            viewHolderConstructor = viewHolderClass.getConstructor(View.class, int.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            throw new IllegalStateException("Constructor of viewHolder might not be public");
        }
    }

    public void setSourceList(ArrayList<S> input) {
        sourceList = input;
        notifyDataSetChanged();
    }

    boolean isHeader(int position) {
        return sourceList.get(position).isHeader;
    }

    int getHeaderPositionForItem(int position) {
        return sourceList.get(position).headerPosition;
    }

    void bindHeaderData(View header, int position) {
        ((TextView)header.findViewById(R.id.header_text)).setText(sourceList.get(position).headerText);
    }

    @NonNull
    @Override
    @SuppressWarnings("ConstantConditions")
    public T onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        try {
            return viewHolderConstructor.newInstance(inflater.inflate(viewType, parent, false), viewType);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(@NonNull T holder, int position) {
        if(sourceList.get(position).isHeader) {
            holder.setHeaderText(sourceList.get(position).headerText);
        } else {
            holder.bindItem(sourceList.get(position), activity);
        }
    }

    @Override
    public int getItemCount() {
        return sourceList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(sourceList.get(position).isHeader) {
            return R.layout.element_header;
        } else {
            return layoutResourceId;
        }
    }
}
