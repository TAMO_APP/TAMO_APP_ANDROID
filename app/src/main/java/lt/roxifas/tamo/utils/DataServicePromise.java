package lt.roxifas.tamo.utils;

import org.jdeferred2.Promise;

import lt.roxifas.tamo.models.DataErrorEnum;

public interface DataServicePromise<T> extends Promise<T, DataErrorEnum, Void> {
}
