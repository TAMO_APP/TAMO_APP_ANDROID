package lt.roxifas.tamo.utils;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import lt.roxifas.tamo.R;
import lt.roxifas.tamo.models.dataItems.ListItemModel;

public class HeaderItemDecoration extends RecyclerView.ItemDecoration {

    private CommonRecyclerAdapter adapter;
    private RecyclerView recyclerView;


    public static<T extends ListItemModel> ArrayList<T> makeHeaders(ArrayList<T> inputList, String emptyPhrase, Class<T> itemClass) {
        T entry;
        int currentHeaderPosition = 0;
        boolean headerCreated = false;
        for (int i = 0; i < inputList.size() ; i++) {
            if(!headerCreated) {
                try {
                    entry = itemClass.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                    return null;
                }
                entry.isHeader = true;
                entry.headerText = inputList.get(i).getHeaderSource();
                if(entry.headerText == null)throw new IllegalArgumentException("The given data model doesn't support headers");
                currentHeaderPosition = i;
                entry.headerPosition = currentHeaderPosition;
                inputList.add(i, entry);
                i++;
                headerCreated = true;
            }
            inputList.get(i).headerPosition = currentHeaderPosition;
            if(i + 1 != inputList.size()) headerCreated = inputList.get(i).getHeaderSource().equals(inputList.get(i + 1).getHeaderSource());
        }

        if(inputList.size() == 0) { //Detect if the current month has no grades - and insert a message to the user this
            try {
                entry = itemClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
            entry.headerText = emptyPhrase;
            entry.headerPosition = 0;
            entry.isHeader = true;
            inputList.add(entry);
        }
        return inputList;
    }

    public HeaderItemDecoration(CommonRecyclerAdapter adapter, RecyclerView recyclerView) {
        this.adapter = adapter;
        this.recyclerView = recyclerView;
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(c, parent, state);

        View topChild = parent.getChildAt(0);
        if (topChild == null) return;

        int topChildPosition = parent.getChildAdapterPosition(topChild);
        if (topChildPosition == RecyclerView.NO_POSITION) return;

        View currentHeader = getHeaderViewForItem(topChildPosition, parent);
        fixLayoutSize(parent, currentHeader);
        int contactPoint = currentHeader.getBottom();
        View childInContact = getChildInContact(parent, contactPoint);
        if (childInContact != null) {
            if (adapter.isHeader(recyclerView.getChildAdapterPosition(childInContact))) {
                moveHeader(c, currentHeader, childInContact);
                return;
            }
        }
        drawHeader(c, currentHeader);
    }

    private View getHeaderViewForItem(int itemPosition, RecyclerView parent) {
        int headerPosition = adapter.getHeaderPositionForItem(itemPosition);
        View header = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_header, parent, false);
        adapter.bindHeaderData(header, headerPosition);
        return header;
    }

    private void drawHeader(Canvas c, View header) {
        c.save();
        c.translate(0, 0);
        header.draw(c);
        c.restore();
    }

    private void moveHeader(Canvas c, View currentHeader, View nextHeader) {
        c.save();
        c.translate(0, nextHeader.getTop() - currentHeader.getHeight());
        currentHeader.draw(c);
        c.restore();
    }

    private View getChildInContact(RecyclerView parent, int contactPoint) {
        View childInContact = null;
        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);
            if (child.getBottom() > contactPoint) {
                if (child.getTop() <= contactPoint) {
                    // This child overlaps the contactPoint
                    childInContact = child;
                    break;
                }
            }
        }
        return childInContact;
    }

    //Properly measures and layouts the top sticky header.
    private void fixLayoutSize(ViewGroup parent, View view) {

        // Specs for parent (RecyclerView)
        int widthSpec = View.MeasureSpec.makeMeasureSpec(parent.getWidth(), View.MeasureSpec.EXACTLY);
        int heightSpec = View.MeasureSpec.makeMeasureSpec(parent.getHeight(), View.MeasureSpec.UNSPECIFIED);

        // Specs for children (headers)
        int childWidthSpec = ViewGroup.getChildMeasureSpec(widthSpec, parent.getPaddingLeft() + parent.getPaddingRight(), view.getLayoutParams().width);
        int childHeightSpec = ViewGroup.getChildMeasureSpec(heightSpec, parent.getPaddingTop() + parent.getPaddingBottom(), view.getLayoutParams().height);

        view.measure(childWidthSpec, childHeightSpec);

        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
    }
}
