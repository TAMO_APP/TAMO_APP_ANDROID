package lt.roxifas.tamo.utils;

import org.jdeferred2.impl.DeferredObject;

import lt.roxifas.tamo.models.DataErrorEnum;

public class DataServiceDeferred<T> extends DeferredObject<T, DataErrorEnum, Void> implements DataServicePromise<T> {
    public void resolveInMain(T resolve) {
        Utils.getInstance().runInMainThread(() -> resolve(resolve));
    }

    public void rejectInMain(DataErrorEnum reject) {
        Utils.getInstance().runInMainThread(() -> reject(reject));
    }

    @Override
    public DataServicePromise<T> promise() {
        return this;
    }


}
