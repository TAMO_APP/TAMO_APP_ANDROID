package lt.roxifas.tamo.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment {

    Calendar calendar;
    DatePickerDialog.OnDateSetListener listener;

    public void setDataSet(Calendar calendar, DatePickerDialog.OnDateSetListener listener) {
        this.calendar = calendar;
        this.listener = listener;

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(calendar == null)throw new NullPointerException();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), listener, year, month, day);
    }

}