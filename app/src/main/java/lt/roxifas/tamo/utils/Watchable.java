package lt.roxifas.tamo.utils;

import java.util.ArrayList;

public class Watchable<T> {
    private T variable = null;
    private ArrayList<Watcher<T>> watchers = new ArrayList<>();

    public interface Watcher<T> {
        void onChange(T value);
    }

    public void onChange(Watcher<T> watcher) {
        watchers.add(watcher);
    }

    public void set(T newValue) {
        if(newValue != null && newValue.equals(variable))return;
        variable = newValue;
        for (Watcher<T> watcher : watchers) {
            watcher.onChange(variable);
        }
    }

    public T get() {
        return variable;
    }
}
