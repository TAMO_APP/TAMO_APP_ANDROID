package lt.roxifas.tamo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

import lt.roxifas.tamo.models.dataItems.PremadeDialogModel;

public class OneTimeDialogLauncher {
    private Activity activity;
    private SharedPreferences preferences;
    private ArrayList<PremadeDialogModel> dialogList = new ArrayList<>();
    private ArrayList<Integer> dialogIds = new ArrayList<>();

    public OneTimeDialogLauncher(Activity activity) {
        this.activity = activity;
        preferences = activity.getSharedPreferences("oneTimeDialogDisplayed", Context.MODE_PRIVATE);
    }

    public void addDialog(PremadeDialogModel dialog, int id) {
        if(!preferences.getBoolean(Integer.toString(id), false)) {
            dialogList.add(dialog);
            dialogIds.add(id);
        }
    }

    private void setDialogShown(int id) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Integer.toString(id), true);
        editor.apply();
    }

    private void popDialog() {
        if(dialogList.isEmpty()) return;
        PremadeDialogModel dialog = dialogList.get(0);
        dialogList.remove(0);
        int dialogId = dialogIds.get(0);
        dialogIds.remove(0);
        dialog.show().done(result -> {
            setDialogShown(dialogId);
            popDialog();
        }).fail(result -> activity.finish());
    }

    public void displayDialogs() {
        if(dialogList.isEmpty()) return;
        popDialog();

    }

}
