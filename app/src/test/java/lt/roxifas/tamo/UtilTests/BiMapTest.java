package lt.roxifas.tamo.UtilTests;

import org.junit.Test;

import lt.roxifas.tamo.utils.BiMap;

import static org.junit.Assert.*;

public class BiMapTest {

    @Test
    public void simpleTest() {
        BiMap<Integer, String> biMap = new BiMap<>();
        biMap.put(0, "zero");
        biMap.put(1, "one");

        assertSame("zero", biMap.get(0));
        assertSame(1, biMap.getKey("one"));
    }

}
