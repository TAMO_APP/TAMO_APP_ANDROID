package lt.roxifas.tamo.ParserTests;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import java.util.ArrayList;

import lt.roxifas.tamo.models.dataItems.GradeModel;

import static org.junit.Assert.*;

public class GradetableParserTest extends ParserTestBase {

    @Test
    public void gradetableVariant1() {
        String html = utils.loadResourceAsString("gradetable_variant1.html");
        Document document = Jsoup.parse(html);
        ArrayList<GradeModel> list = parsingService.gradetable(document, "2018-11");
        assertEquals(44, list.size());
        commonTest(list);
    }

    @Test
    public void gradetableVariant2() {
        String html = utils.loadResourceAsString("gradetable_variant2.html");
        Document document = Jsoup.parse(html);
        ArrayList<GradeModel> list = parsingService.gradetable(document, null);
        assertEquals(44, list.size());
        assertEquals("2018-11-05", list.get(0).date);
        commonTest(list);
    }

    private void commonTest(ArrayList<GradeModel> list) {
        assertEquals("nk", list.get(0).grade);
        assertEquals("10", list.get(1).grade);
        assertEquals("nt", list.get(2).grade);
        assertEquals("nt", list.get(3).grade);
        assertEquals("nt", list.get(4).grade);
        assertEquals("np", list.get(5).grade);
        assertEquals("nt", list.get(6).grade);
        assertEquals("nt", list.get(7).grade);
        assertEquals("10", list.get(8).grade);
        assertEquals("nt", list.get(9).grade);
        assertEquals("nt", list.get(10).grade);
        assertEquals("nt", list.get(11).grade);
        assertEquals("nt", list.get(12).grade);
        assertEquals("nt", list.get(13).grade);
        assertEquals("nt", list.get(14).grade);
        assertEquals("5", list.get(15).grade);
        assertEquals("nt", list.get(16).grade);
        assertEquals("nt", list.get(17).grade);
        assertEquals("nt", list.get(18).grade);
        assertEquals("nt", list.get(19).grade);
        assertEquals("nt", list.get(20).grade);
        assertEquals("nt", list.get(21).grade);
        assertEquals("nt", list.get(22).grade);
        assertEquals("9", list.get(23).grade);
        assertEquals("8", list.get(24).grade);
        assertEquals("9", list.get(25).grade);
        assertEquals("nt", list.get(26).grade);
        assertEquals("nt", list.get(27).grade);
        assertEquals("nt", list.get(28).grade);
        assertEquals("nt", list.get(29).grade);
        assertEquals("nt", list.get(30).grade);
        assertEquals("nt", list.get(31).grade);
        assertEquals("10", list.get(32).grade);
        assertEquals("nt", list.get(33).grade);
        assertEquals("9", list.get(34).grade);
        assertEquals("9", list.get(35).grade);
        assertEquals("nt", list.get(36).grade);
        assertEquals("nt", list.get(37).grade);
        assertEquals("nt", list.get(38).grade);
        assertEquals("nt", list.get(39).grade);
        assertEquals("nt", list.get(40).grade);
        assertEquals("nt", list.get(41).grade);
        assertEquals("nt", list.get(42).grade);
        assertEquals("nt", list.get(43).grade);
    }

}
