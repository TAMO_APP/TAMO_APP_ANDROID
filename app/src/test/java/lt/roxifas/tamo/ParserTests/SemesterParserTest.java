package lt.roxifas.tamo.ParserTests;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import java.util.ArrayList;

import lt.roxifas.tamo.models.dataItems.SemesterModel;

import static org.junit.Assert.assertEquals;

public class SemesterParserTest extends ParserTestBase {

    @Test
    public void simpleTest() {
        String html = utils.loadResourceAsString("semesters.html");
        Document document = Jsoup.parse(html);
        ArrayList<SemesterModel> list = parsingService.semesters(document);
        assertEquals(12, list.size());
        assertEquals("Subject6 - 4,5", list.get(2).subjectAverages);
        assertEquals("5 4", list.get(2).grades);
    }

}
