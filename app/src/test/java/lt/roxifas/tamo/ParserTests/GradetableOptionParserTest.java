package lt.roxifas.tamo.ParserTests;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import java.util.ArrayList;

import lt.roxifas.tamo.models.dataItems.GradeModel;

import static org.junit.Assert.assertEquals;

public class GradetableOptionParserTest extends ParserTestBase {

    @Test
    public void simpleTest() {
        String html = utils.loadResourceAsString("gradetableOptions.html");
        Document document = Jsoup.parse(html);
        ArrayList<String> list = parsingService.gradetableOptions(document);
        assertEquals(3, list.size());
        assertEquals("2018-09",list.get(0));
        assertEquals("2018-10",list.get(1));
        assertEquals("2018-11",list.get(2));
    }

}
