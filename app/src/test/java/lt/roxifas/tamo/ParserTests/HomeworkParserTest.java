package lt.roxifas.tamo.ParserTests;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import java.util.ArrayList;

import lt.roxifas.tamo.models.dataItems.HomeworkModel;

import static org.junit.Assert.*;

public class HomeworkParserTest extends ParserTestBase {

    @Test
    public void emptyList() {
        String html = utils.loadResourceAsString("homeworkEmpty.html");
        Document document = Jsoup.parse(html);
        ArrayList<HomeworkModel> list = parsingService.homework(document);
        assertEquals(0, list.size());
    }

}
