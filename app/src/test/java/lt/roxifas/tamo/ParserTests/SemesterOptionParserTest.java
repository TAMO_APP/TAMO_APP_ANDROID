package lt.roxifas.tamo.ParserTests;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import java.util.ArrayList;

import lt.roxifas.tamo.models.dataItems.NameIdPair;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SemesterOptionParserTest extends ParserTestBase{

    @Test
    public void simpleTest() {
        String html = utils.loadResourceAsString("semesterOptions.html");
        Document document = Jsoup.parse(html);
        ArrayList<NameIdPair> list = parsingService.semesterOptions(document);
        assertEquals(3, list.size());
        assertEquals("1", list.get(0).id);
        assertEquals("2 pusmetis", list.get(1).name);
        assertTrue(list.get(0).selected);
        assertFalse(list.get(1).selected);
    }

}
