package lt.roxifas.tamo.ParserTests;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import java.util.ArrayList;

import lt.roxifas.tamo.models.dataItems.NoteModel;

import static org.junit.Assert.*;

public class NotesParserTest extends ParserTestBase {

    @Test
    public void simpleTest() {
        String html = utils.loadResourceAsString("notes.html");
        Document document = Jsoup.parse(html);
        ArrayList<NoteModel> list = parsingService.notes(document);
        assertEquals(5, list.size());
        for(NoteModel note : list) {
            assertTrue(note.teachersName.matches("Teacher[0-9]+"));
            assertEquals("Note text", note.entryText);
        }

    }

}
