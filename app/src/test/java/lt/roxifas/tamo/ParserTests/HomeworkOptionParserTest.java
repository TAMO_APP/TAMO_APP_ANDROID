package lt.roxifas.tamo.ParserTests;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import java.util.ArrayList;

import lt.roxifas.tamo.models.dataItems.NameIdPair;

import static org.junit.Assert.*;

public class HomeworkOptionParserTest extends ParserTestBase {

    @Test
    public void listOf6() {
        String html = utils.loadResourceAsString("homeworkOptions6.html");
        Document document = Jsoup.parse(html);
        ArrayList<NameIdPair> list = parsingService.homeworkOptions(document);
        assertEquals(6, list.size());
        assertTrue(optionIdCheck(list));
        assertTrue(optionNameCheck(list));
    }

    @Test
    public void listOf12() {
        String html = utils.loadResourceAsString("homeworkOptions12.html");
        Document document = Jsoup.parse(html);
        ArrayList<NameIdPair> list = parsingService.homeworkOptions(document);
        assertEquals(12, list.size());
        assertTrue(optionIdCheck(list));
        assertTrue(optionNameCheck(list));
    }

    private boolean optionNameCheck(ArrayList<NameIdPair> list) {
        for (int i = 0 ; i < list.size() ; i++) {
            if(!list.get(i).name.equals("Subject" + i)) {
                return false;
            }
        }
        return true;
    }

    private boolean optionIdCheck(ArrayList<NameIdPair> list) {
        for (int i = 0 ; i < list.size() ; i++) {
            if(!list.get(i).id.equals("Id" + i)) {
                return false;
            }
        }
        return true;
    }
}
