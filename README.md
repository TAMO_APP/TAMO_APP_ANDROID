# A free android application for accessing the Lithuanian tamo.lt school system
# Not actively maintained anymore!

[![pipeline status](https://gitlab.com/TAMO_APP/TAMO_APP_ANDROID/badges/master/pipeline.svg)](https://gitlab.com/TAMO_APP/TAMO_APP_ANDROID/commits/master)
[![coverage report](https://gitlab.com/TAMO_APP/TAMO_APP_ANDROID/badges/master/coverage.svg)](https://gitlab.com/TAMO_APP/TAMO_APP_ANDROID/commits/master)

## Features:
* Homework
* Grades of month
* Semester grades
* Messages (with multi-delete and support for attachments)
* Timetable
* Holiday dates display
* Notes
* Tests
* Offline temporary storage for data 

## Some notes about the code:
* Data is extracted by using the [web scraping](https://en.wikipedia.org/wiki/Web_scraping) technique
* Separate functionalities of the app (pages) are in the [sections](https://gitlab.com/TAMO_APP/TAMO_APP_ANDROID/tree/master/app/src/main/java/lt/roxifas/tamo/sections) folder
* If you plan on maintaning this code yourself, some things that are not perfect and could be improved are:
  * Cleaning up the start-up routines in `MainActivity`, while the current code there allows for a blazing fast launch of the app, it's not very readable
  * Improving the way data is passed around, maybe use something like Redux and make a global app state object
  * There are probably more things that I missed, keep in mind that a lot of this code was written when I wasn't very experienced :)
* If you're only interested in fixing a data parser that was broken by a change in the tamo system, you can find all of the HTML parsing code in [this file](https://gitlab.com/TAMO_APP/TAMO_APP_ANDROID/blob/master/app/src/main/java/lt/roxifas/tamo/services/ParsingService.java)

## Downloads:
* [Google play link](https://play.google.com/store/apps/details?id=lt.roxifas.tamo)
* [Direct download of apk package for any release](https://gitlab.com/TAMO_APP/TAMO_APP_ANDROID/tags)

## Latest changes (v0.5.10):
* Update homework parser

## License:
[GNU GPL v3](LICENSE)
